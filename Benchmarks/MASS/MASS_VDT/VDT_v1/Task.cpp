#include "Task.h"

using namespace std;


Task::Task() {
  type = priority = totalHours = taskId = projectId = collabStartDay = -1;
}


Task::Task(int type_, int priority_, int totalHours_, int taskId_) {
  type = type_;
  priority = priority_;
  totalHours = totalHours_;
  taskId = taskId_;
  collabStartDay = projectId = -1;
}


Task::Task(Task* toCopy) {
  type = toCopy->type;
  priority = toCopy->priority;
  totalHours = toCopy->totalHours;
  taskId = toCopy->taskId;
  projectId = toCopy->projectId;
  collabStartDay = toCopy->collabStartDay;
  for (int hr = 0; hr < 5; hr++)
    hours[hr] = toCopy->hours[hr];
}


bool Task::makeProgress(int engType) {
  if (hours[engType] > 0) {
    hours[engType] -= 1;
    totalHours -= 1;
    return true;
  }
  else {
    return false;
  }
}


/*================================= Getters ===============================*/

int Task::getType() const {
  return type;
}

int Task::getPriority() const {
  return priority;
}

int Task::getTotalHours() const {
  return totalHours;
}

int Task::getTaskId() const {
  return taskId;
}

int Task::getProjectId() const {
  return projectId;
}

int Task::getCollabDay() const {
  return collabStartDay;
}

int Task::getEngrHours(int engrLvl) const {
  return hours[engrLvl];
}


/*================================= Setters ===============================*/
void Task::setType(int type_) {
  type = type_;
}

void Task::setPriority(int priority_) {
  priority = priority_;
}

void Task::setTotalHours(int totalHours_) {
  totalHours = totalHours_;
}

void Task::setTaskId(int taskId_) {
  taskId = taskId_;
}

void Task::setProjectId(int projId) {
  projectId = projId;
}

void Task::setCollabDay(int day) {
  collabStartDay = day;
}

bool Task::setHours(int engineerLevel, int numHours) {
  if (engineerLevel >= 0 && engineerLevel < 5) {
    hours[engineerLevel] = numHours;
    return true;
  }
  else {
    return false;
  }
}


