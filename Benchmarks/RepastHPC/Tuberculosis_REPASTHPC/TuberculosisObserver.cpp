//
// Created by sarah
//

#include "TuberculosisObserver.h"

void TuberculosisObserver::setup(repast::Properties &prop) {

    macrophageType = create<Macrophage>(MACROPHAGE_COUNT);

    if (_rank == 0) {

        // move Macros to random starting position
        macroStartPositions();

        // set 4 LungPlaces as the entry points / blood vessels for new TCells and Macrophages
        // (located at the center of each quadrant)
        setBloodVessels();

        // create 4 invisible spawner Macros to sit at the blood vessels
        createMacroSpawners();
        // create 4 invisible spawner TCells to sit at the blood vessels
        createTCellSpawners();

        // set bacteria spawn points - 4 bacteria located in the center of the grid
        spawnInitialBacteria();
    }
}

void TuberculosisObserver::macroStartPositions() {
    repast::relogo::AgentSet<LungPlace> lungPlaces;
    patches(lungPlaces);
    lungPlaces.shuffle();

    repast::relogo::AgentSet<Macrophage> macrophages;
    get(macrophages);

    for (int i = 0; i < MACROPHAGE_COUNT; i++) {
        macrophages[i]->moveTo(lungPlaces[i]);
    }
}

void TuberculosisObserver::setBloodVessels() {
    // assumes a square, discrete simulation space
    int lowIndex = (maxPxcor() / 2) / 2;
    int highIndex = maxPxcor() - lowIndex; // TODO: is this right?

    bloodVessels.add(patchAt<LungPlace>(lowIndex, lowIndex));
    bloodVessels.add(patchAt<LungPlace>(lowIndex, highIndex));
    bloodVessels.add(patchAt<LungPlace>(highIndex, lowIndex));
    bloodVessels.add(patchAt<LungPlace>(highIndex, highIndex));

    bloodVessels.ask(&LungPlace::setBloodVessel);
}

void TuberculosisObserver::spawnInitialBacteria() {
    // assumes a square, discrete simulation space
    int highIndex = maxPxcor() / 2;
    int lowIndex = highIndex - 1;

    repast::relogo::AgentSet<LungPlace> bSpawnPts;
    bSpawnPts.add(patchAt<LungPlace>(lowIndex, lowIndex));
    bSpawnPts.add(patchAt<LungPlace>(lowIndex, highIndex));
    bSpawnPts.add(patchAt<LungPlace>(highIndex, lowIndex));
    bSpawnPts.add(patchAt<LungPlace>(highIndex, highIndex));

    bSpawnPts.ask(&LungPlace::spawnBacteria);
}

void TuberculosisObserver::createMacroSpawners() {
    repast::relogo::AgentSet<Macrophage> macrophages;
    get(macrophages);

    repast::relogo::AgentSet<Macrophage> macroSpawners;
    // hatch
    for (int i = 0; i < bloodVessels.size(); i++) {
        Macrophage *parent = macrophages.oneOf();
        // TODO: Check that spawner Macrophages are created correctly
        Macrophage *spawner = hatch<Macrophage>(parent);
        macroSpawners.add(spawner);
    }
    macroSpawners.ask(&Macrophage::setToSpawner);

    // migrate to entry points
    for (int i = 0; i < bloodVessels.size(); i++) {
        macroSpawners[i]->moveTo(bloodVessels[i]);
    }
}

void TuberculosisObserver::createTCellSpawners() {
    // create TCells
    tCellType = create<TCell>(bloodVessels.size());

    repast::relogo::AgentSet<TCell> tCells;
    get(tCells);
    tCells.ask(&TCell::setToSpawner);

    // migrate to entry points
    for (int i = 0; i < bloodVessels.size(); i++) {
        tCells[i]->moveTo(bloodVessels[i]);
    }
}

void TuberculosisObserver::go() {

    int day = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
    if (_rank == 0) {
        cout << "Iteration : " << day << endl;
    }

    fstream outFile;
    startStateFile(day);

    // cross process synchronization
    synchronize<AgentPackage>(*this, *this, *this, repast::RepastProcess::USE_LAST_OR_USE_CURRENT);

    // spawn new Macrophages and TCells through blood vessels (entry points)
    spawnThroughVessels();

    repast::relogo::AgentSet<LungPlace> places;
    patches(places);
    // advance time - decay chemokine signals every time tick and grow bacteria radially by one unit every 10th tick
    places.ask(&LungPlace::decayChemokineAndGrowBacteria);
    places.ask(&LungPlace::updateNextState);

    // Macrophages move toward places having the highest chemokine levels
    repast::relogo::AgentSet<Macrophage> macrophages;
    get(macrophages);

    macrophages.ask(&Macrophage::move);
    macrophages.ask(&Macrophage::collisionFreeJumpBack);

    // TCells move toward places having the highest chemokine levels
    repast::relogo::AgentSet<TCell> tCells;
    get(tCells);
    tCells.ask(&TCell::move);
    tCells.ask(&TCell::collisionFreeJumpBack);


    macrophages.ask(&Macrophage::react);
    places.ask(&LungPlace::reactToMacrophages);

    // write states out to file
    places.ask(&LungPlace::writeState);
    macrophages.ask(&Macrophage::writeState);
    tCells.ask(&TCell::writeState);


    // remove dead Macrophages from simulation
    macrophages.ask(&Macrophage::removeDeadMacrophages);

    // close file and redirect c_out stream after this iteration is done writing out
    endStateFile(day);
}

void TuberculosisObserver::spawnThroughVessels() {
    for (LungPlace *vessel : bloodVessels) {
        spawnHelper(vessel);
    }
}

/**
 *
 * @param today - current time tick
 * @param place - assumes this is a blood vessel place that spawns new Macrophages and TCells
 */
void TuberculosisObserver::spawnHelper(LungPlace *place) {
    int today = place->getCurrentTick();
    bool hasMacro = place->turtlesHere<Macrophage>().size() > 1; // assumes invisible spawner Macro is here
    bool hasTCell = place->turtlesHere<TCell>().size() > 1; // assumes invisible spawner TCell is here

    // if there are max number of immune cells on this Place, don't recruit any new cells
    if (hasMacro && hasTCell) {
        return;

    } else if (!hasTCell && hasMacro) {
        if (today >= tCellEntrance) {
            hatch<TCell>(place->turtlesHere<TCell>().oneOf()); // assumes only TCell on this patch is a spawner
            return;
        } else {
            return;
        }

    } else if (hasTCell) {
        hatch<Macrophage>(place->turtlesHere<Macrophage>().oneOf()); // assumes only Macro on this patch is a spawner
        return;

    } else { // no immune cells at this current Place
        if ((today >= tCellEntrance) && (rand() & 1) == 1) {
            hatch<TCell>(place->turtlesHere<TCell>().oneOf());
            return;

        } else {
            hatch<Macrophage>(place->turtlesHere<Macrophage>().oneOf());
            return;
        }
    }
}

repast::relogo::RelogoAgent *TuberculosisObserver::createAgent(const AgentPackage &content) {
    int toCreate = content.type;

    if (toCreate == macrophageType) {
        return new Macrophage(content.getId(), this, content);
    } else if (toCreate == tCellType) {
        return new TCell(content.getId(), this, content);
    } else { // assuming this is a LungPlace
        return new LungPlace(content.getId(), this, content);
    }
}

void TuberculosisObserver::createAgents(vector<AgentPackage> &contents, vector<repast::relogo::RelogoAgent *>
&outgoing) {

    size_t cSize = contents.size();

    for (size_t i = 0; i < cSize; i++) {
        int cType = contents[i].type;

        if (cType == macrophageType) {
            outgoing.push_back(new Macrophage(contents[i].getId(), this, contents[i]));

        } else if (cType == tCellType) {
            outgoing.push_back(new TCell(contents[i].getId(), this, contents[i]));

        } else {
            outgoing.push_back(new LungPlace(contents[i].getId(), this, contents[i]));
        }
    }
}


void TuberculosisObserver::provideContent(const repast::AgentRequest &request, vector<AgentPackage> &out) {
    const vector<repast::AgentId> &reqIds = request.requestedAgents();
    int numIds = reqIds.size();

    for (int i = 0; i < numIds; i++) {
        repast::AgentId id = reqIds[i];

        AgentPackage content = {id.id(), id.startingRank(), id.agentType(), id.currentRank(), false, -1, -1, 0, -1,
                                false};

        if (id.agentType() == macrophageType) {
            Macrophage *macrophage = who<Macrophage>(id);
            content.dayInfected = macrophage->getDayInfected();
            content.intracellularBacteria = macrophage->getIntracellularBacteria();
            content.state = static_cast<int>(macrophage->getState());
            content.spawner = macrophage->isSpawner();
            // content = {id.id(), id.startingRank(), id.agentType(), id.currentRank(),
            //            false, -1,
            //            macrophage->getDayInfected(), macrophage->getIntracellularBacteria(),
            //            Macrophage::RESTING, macrophage->isSpawner()};

        } else if (id.agentType() == tCellType) {
            TCell *tCell = who<TCell>(id);
            content.spawner = tCell->isSpawner();
//            content = {id.id(), id.startingRank(), id.agentType(), id.currentRank(),
//                       false, -1,
//                       -1, 0, Macrophage::RESTING, tCell->isSpawner()};

        } else {
            LungPlace *lungPlace = who<LungPlace>(id);
            content.bacteria = lungPlace->hasBacteria();
            content.chemokine = lungPlace->getChemokine();
//            content = {id.id(), id.startingRank(), id.agentType(), id.currentRank(),
//                       lungPlace->hasBacteria(), lungPlace->getChemokine(),
//                       -1, 0, Macrophage::RESTING, false};
        }
        out.push_back(content);
        //out.push_back(provideContentHelp(id));
    }
}

void TuberculosisObserver::provideContent(repast::relogo::RelogoAgent *agent, vector<AgentPackage> &outgoing) {
    repast::AgentId id = agent->getId();
    AgentPackage content = {id.id(), id.startingRank(), id.agentType(), id.currentRank(), false, -1, -1, 0, -1, false};

    if (id.agentType() == macrophageType) {
        Macrophage *macrophage = static_cast<Macrophage *>(agent);
        repast::AgentId id = macrophage->getId();
        content.dayInfected = macrophage->getDayInfected();
        content.intracellularBacteria = macrophage->getIntracellularBacteria();
        content.state = static_cast<int>(macrophage->getState());
        content.spawner = macrophage->isSpawner();
//        content = {id.id(), id.startingRank(), id.agentType(), id.currentRank(),
//                   false, -1,
//                   macrophage->getDayInfected(), macrophage->getIntracellularBacteria(),
//                   Macrophage::RESTING, macrophage->isSpawner()};

    } else if (id.agentType() == tCellType) {
        TCell *tCell = static_cast<TCell *>(agent);
        repast::AgentId id = tCell->getId();
        content.spawner = tCell->isSpawner();
//        content = {id.id(), id.startingRank(), id.agentType(), id.currentRank(),
//                   false, -1,
//                   -1, 0, Macrophage::RESTING, tCell->isSpawner()};

    } else {
        LungPlace *lungPlace = static_cast<LungPlace *>(agent);
        repast::AgentId id = lungPlace->getId();
        content.bacteria = lungPlace->hasBacteria();
        content.chemokine = lungPlace->getChemokine();
    }
    outgoing.push_back(content);
    //outgoing.push_back(provideContentHelp(id));
}

AgentPackage TuberculosisObserver::provideContentHelp(repast::AgentId id) {

    AgentPackage content;

    if (id.agentType() == macrophageType) {
        Macrophage *macrophage = who<Macrophage>(id);
        content = {id.id(), id.startingRank(), id.agentType(), id.currentRank(),
                   false, -1,
                   macrophage->getDayInfected(), macrophage->getIntracellularBacteria(),
                   Macrophage::RESTING, macrophage->isSpawner()};

    } else if (id.agentType() == tCellType) {
        TCell *tCell = who<TCell>(id);
        content = {id.id(), id.startingRank(), id.agentType(), id.currentRank(),
                   false, -1,
                   -1, 0, Macrophage::RESTING, tCell->isSpawner()};

    } else {
        LungPlace *lungPlace = who<LungPlace>(id);
        content = {id.id(), id.startingRank(), id.agentType(), id.currentRank(),
                   lungPlace->hasBacteria(), lungPlace->getChemokine(),
                   -1, 0, Macrophage::RESTING, false};
    }
    return content;
}


void TuberculosisObserver::updateAgent(AgentPackage content) {
    repast::AgentId id(content.id, content.proc, content.type);

    if (id.agentType() == macrophageType) {
        Macrophage *macrophage = who<Macrophage>(id);
        macrophage->setDayInfected(content.dayInfected);
        macrophage->setIntracellularBacteria(content.intracellularBacteria);
        macrophage->setState(static_cast<Macrophage::State>(content.state));
        macrophage->setSpawner(content.spawner);

    } else if (id.agentType() == tCellType) {
        TCell *tCell = who<TCell>(id);
        tCell->setSpawner(content.spawner);

    } else {
        LungPlace *lungPlace = who<LungPlace>(id);
        lungPlace->setBacteria(content.bacteria);
        lungPlace->setChemokine(content.chemokine);
    }
}

void TuberculosisObserver::startStateFile(int day) {
    file.open("XML_states_Rank_" + to_string(_rank) + "/" + to_string(day) + ".xml", ios::out);
    string line;

    // Backup stream buffers of  c_out
    stream_buff_out = cout.rdbuf();

    // Get the stream buffer of the file
    streambuf *stream_buffer_file = file.rdbuf();

    // Redirect c_out to file
    cout.rdbuf(stream_buffer_file);
    cout << "<states>" << endl;
}

void TuberculosisObserver::endStateFile(int day) {
    cout << "</states>" << endl;

    cout.rdbuf(stream_buff_out);
    cout << "Finished writing states for day " << day << endl;

    file.close();
}













