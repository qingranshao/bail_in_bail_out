#!/usr/bin/python
import argparse
from random import shuffle

from place import Place
from macrophage import Macrophage
from agent_factory import Agent_Factory
from day_tracker import Day_Tracker

from helper_functions import calculate_neighbors
from helper_functions import to_id
from helper_functions import blood_vessels 
from helper_functions import bacteria_spawn_points

# Take user arguments
DEFAULT_OUTFILE='0.xml'

parser = argparse.ArgumentParser(description='Create a initial state file')
parser.add_argument('size', type=int, help='Size of one side of our square grid')
parser.add_argument('--outfile', type=str,  default=DEFAULT_OUTFILE, help='File to output. defaults to: {}'.format(DEFAULT_OUTFILE))

args = parser.parse_args()

total_size = args.size ** 2

# open the file 0.xml
with open(args.outfile, 'w') as f:
    f.write('<states>\n')
    f.write('<itno>0</itno>\n')

    # create the environmental constant grid size
    f.write("""<environment>\n<grid_size>{size}</grid_size>\n</environment>""".format(size=args.size))

    # create places -----------------------------------------------------------

    all_places = []
    blood_vessel_ids = blood_vessels(args.size);
    bacteria_spawn_ids = bacteria_spawn_points(args.size);

    for j in range(args.size):
        for i in range(args.size):
            
            if to_id(i, j, args.size) in blood_vessel_ids:
                # construct Place(id, x, y, bacteria, blood_vessel, place_neighbors)
                p = Place(to_id(i, j, args.size), i, j, 0, 1, calculate_neighbors(i, j, args.size))

            else:
                p = Place(to_id(i, j, args.size), i, j, 0, 0, calculate_neighbors(i, j, args.size))

            if p.id in bacteria_spawn_ids:
                # set bacteria at spawn points
                p.spawn_bacteria()
                
            # add to the list of places
            all_places.append(p)


    # create macrophages ------------------------------------------------------

    # shuffle all_places to randomly spawn macrophages over grid of Places
    shuffle(all_places)

    all_macrophages = []

    for k, place in enumerate(all_places[:100]):
        
        # construct Macrophage(id, x, y, neighbors, side_length)
        m = Macrophage(k, place.x, place.y, calculate_neighbors(place.x, place.y, args.size))

        # set macrophage in corresponding place
        place.place_macrophage();

        all_macrophages.append(m);

    # write out all places and macrophages
    all_places.sort(key=lambda place:place.id)

    for place in all_places:
        f.write(str(place))

    for macrophage in all_macrophages:
        f.write(str(macrophage))

    # create Agent Factory ----------------------------------------------------
    a = Agent_Factory()

    f.write(str(a))

    # create Day Tracker ------------------------------------------------------
    d = Day_Tracker()

    f.write(str(d))


    f.write('\n</states>')

# close file


