

#include "Life.h"
#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/Point.h"

RepastHPCAgent::RepastHPCAgent(repast::AgentId id): id_(id), alive(200){ }

RepastHPCAgent::RepastHPCAgent(repast::AgentId id, bool newAlive): id_(id), alive(newAlive){ }

RepastHPCAgent::~RepastHPCAgent(){ }


void RepastHPCAgent::set(int currentRank, bool newAlive){
    id_.currentRank(currentRank);
    alive = rand() %2;
}


void RepastHPCAgent::play(repast::SharedContext<RepastHPCAgent>* context,
                              repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space){
    std::vector<RepastHPCAgent*> agentsToPlay;
    
    std::vector<int> agentLoc;
    space->getLocation(id_, agentLoc);
    repast::Point<int> center(agentLoc);
    repast::Moore2DGridQuery<RepastHPCAgent> moore2DQuery(space);
    moore2DQuery.query(center, 1, false, agentsToPlay);
    
    
    double totalAlive = alive;
    std::vector<RepastHPCAgent*>::iterator agentToPlay = agentsToPlay.begin();
    while(agentToPlay != agentsToPlay.end()){
        std::vector<int> otherLoc;
        space->getLocation((*agentToPlay)->getId(), otherLoc);
        repast::Point<int> otherPoint(otherLoc);

        totalAlive += (*agentToPlay)->getAlive();

		
        agentToPlay++;
    }
    if (totalAlive == 3) { //next generation will be life
       alive = 1;
    }
    else if (totalAlive > 4 || totalAlive < 3) { //next generation will be the same as current state
       alive = 0;
    }

    std::cout << " AGENT " << id_ << " Alive status: " << alive << std::endl;
	
}




/* Serializable Agent Package Data */

RepastHPCAgentPackage::RepastHPCAgentPackage(){ }

RepastHPCAgentPackage::RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank, bool _alive):
id(_id), rank(_rank), type(_type), currentRank(_currentRank), alive(_alive){ }
