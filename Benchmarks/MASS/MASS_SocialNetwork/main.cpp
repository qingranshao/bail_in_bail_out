
#include "MASS.h"
#include "SNetwork.h"
#include "Timer.h"	//Timer
#include <stdlib.h> // atoi
#include <unistd.h>
#include <vector>
#include <string>

const int iterPerTurn = 9;						
const bool printOutput = true;

Timer timer;

int main( int argc, char *args[] ) {

	// check that all arguments are present 
	// exit with an error if they are not
	if ( argc != 10 ) {
		cerr << "usage: ./main username password machinefile port nProc nThreads numTurns sizeX sizeY" << endl;
		return -1;
	}
  
	// get the arguments passed in
	char *arguments[4];
	arguments[0] = args[1]; // username
	arguments[1] = args[2]; // password
	arguments[2] = args[3]; // machinefile
	arguments[3] = args[4]; // port
	int nProc = atoi( args[5] ); // number of processes
	int nThr = atoi( args[6] );  // number of threads
  
	const int numTurns = atoi( args[7] );	//Run this simulation for numTurns
	const int sizeX = atoi( args[8] );
	const int sizeY = atoi( args[9] );
	const double rmSpawn = 0.2;
	const int myPopulation = sizeX * sizeY * rmSpawn * rmSpawn;	//Population{RMsize, RMSpawn}
 	static const int totalSize = sizeX * sizeY;							//Total size of map

	// initialize MASS with the machine information,
	// number of processes, and number of threads
	MASS::init( arguments, nProc, nThr ); 

	// prepare a message for the places (this is argument below)
	char *msg = (char *)("hello\0"); // should not be char msg[]
  
	/*  THIS SECTION OF CODE DEALS ONLY WITH PLACES  */
  
	// Create the places.
	// Arguments are, in order:
	//    handle, className, boundary_width, argument, argument_size, dim, ...
	Places *snetwork = new Places( 1, "SNetwork", 1, msg, 6, 2, sizeX, sizeY );

   vector<int*> destinations;

   int numVariables = ((sizeX-1) * 4) * sizeX;
   int variableCounter = 0;
   int directions[numVariables][2];

   for (int j = 1; j <= (sizeX - 1); j++) {
      directions[variableCounter][0] = 0;
      directions[variableCounter][1] = j;
      destinations.push_back(directions[variableCounter]);
      variableCounter++;

      directions[variableCounter][0] = j;
      directions[variableCounter][1] = 0;
      destinations.push_back(directions[variableCounter]);
      variableCounter++;

      directions[variableCounter][0] = 0;
      directions[variableCounter][1] = -j;
      destinations.push_back(directions[variableCounter]);
      variableCounter++;

      directions[variableCounter][0] = -j;
      directions[variableCounter][1] = 0;
      destinations.push_back(directions[variableCounter]);
      variableCounter++;
      for (int k = 1; k <= (sizeY - 1); k++) {


         directions[variableCounter][0] = -j;
         directions[variableCounter][1] = k;
         destinations.push_back(directions[variableCounter]);
         variableCounter++;

         directions[variableCounter][0] = j;
         directions[variableCounter][1] = k;
         destinations.push_back(directions[variableCounter]);
         variableCounter++;

         directions[variableCounter][0] = -j;
         directions[variableCounter][1] = -k;
         destinations.push_back(directions[variableCounter]);
         variableCounter++;

         directions[variableCounter][0] = j;
         directions[variableCounter][1] = -k;
         destinations.push_back(directions[variableCounter]);
         variableCounter++;
      }
   }

  
   int* numPlaces = (int *)(sizeX * sizeY);
	snetwork->callAll(SNetwork::init_, numPlaces, sizeof(int));				//initialize life

   srand(time(NULL));
	timer.start();
	


	for(int turn = 0; turn < numTurns; turn++)
	{
      //int degree = rand() % sizeX;
      int degree = 4;
      if (degree == 0) {
         degree = 1;
      }
      
      //exchanges each "persons" 1st degree of separation friends
      snetwork->exchangeAll(1, SNetwork::exchangeMessage_, &destinations);
      
      snetwork->callAll(SNetwork::checkInMessage_, (int *)degree, sizeof(int));

      //resets the inMessage[]
      snetwork->callAll(SNetwork::resetInMessages_);

      destinations.clear();
	}
	
	std::cerr << "Health Status after " << numTurns << std::endl;
	int* maxTurns = new int;
	*maxTurns = numTurns;
	
	long elaspedTime_END =  timer.lap();
	printf( "\nEnd of simulation. Elasped time using MASS framework with %i processes and %i thread and %i turns :: %ld \n",nProc,nThr,numTurns, elaspedTime_END);
  
	MASS::finish( );
}
