//
// Created by sarah
//

#ifndef TCELL_H
#define TCELL_H

#include "relogo/Turtle.h" // derived class of Turtle (mobile agent)
#include "AgentPackage.h"
#include "LungPlace.h"

class TCell : public repast::relogo::Turtle {

public:

// State functions

    void move();

    void collisionFreeJumpBack();

    void writeState();

    // Constructors and destructor

    TCell(const repast::AgentId& id, repast::relogo::Observer *obs) : repast::relogo::Turtle(id, obs), _spawner(false){}

    // For TCell reconstruction after serialization and migration using AgentPackage
    TCell(const repast::AgentId id, repast::relogo::Observer *obs, const AgentPackage &package) :
          repast::relogo::Turtle(id, obs), _spawner(package.spawner) {}

    virtual ~TCell() {}

// Getters and setters
    bool isSpawner() const {
        return _spawner;
    }

    void setToSpawner() {
        _spawner = true;
    }

    void setSpawner(bool isSpawner) {
        _spawner = isSpawner;
    }


private:

    bool _spawner; // if this TCell is an invisible spawner that takes up no space (resides on blood vessels)

    int previousPlaceCoords[2]{-1, -1}; // [x,y]; to enable jump-back for collision free migration
};


#endif // TCELL_H
