/**
 * @author Sarah Panther
 * @since 8-19-19
 */

public class Macrophage extends Agent {

    private int onPlaceID;
    private int state;

    private int intracellularBacteria;
    Macrophage(int m_id, int placeID, int state, int intracellularBacteria) {
        super(m_id);
        this.onPlaceID = placeID;
        this.state = state;
        this.intracellularBacteria = intracellularBacteria;
    }

    public int getOnPlaceID() {
        return onPlaceID;
    }

    public int getState() {
        return state;
    }

    @Override
    public String toString() {
        return state + "\n" + intracellularBacteria + "\n";
    }
}
