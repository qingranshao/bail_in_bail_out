
/*
*agent functions
*/

#include "header.h"
#include "life_agent_header.h"

/**
 * writing alive agents on message board
 * @return [description]
 */
int write_state()
{
  if (STATE == 1) {
    add_state_message(ID, I, J, STATE);
  }
  return 0;
}

/**
 * read and find alive neighbors and increase count
 * @return 0
 */
int read_states()
{

  int mes_i, mes_j, mes_state;


  //start reading the messageboard
  START_STATE_MESSAGE_LOOP;

  //is it another agent?
  if (state_message->id != ID) {
    mes_i = state_message->i;
    mes_j = state_message->j;

    //check if it is a neighbor
    if (((mes_i == I - 1) && (mes_j == J - 1)) ||
        ((mes_i == I - 1) && (mes_j == J)) ||
        ((mes_i == I - 1) && (mes_j == J + 1)) ||
        ((mes_i == I) && (mes_j == J - 1)) ||
        ((mes_i == I) && (mes_j == J + 1)) ||
        ((mes_i == I + 1) && (mes_j == J - 1)) ||
        ((mes_i == I + 1) && (mes_j == J)) ||
        ((mes_i == I + 1) && (mes_j == J + 1))) {

          //add neighbor state to neighbors
          mes_state = state_message->state;
          if (mes_state == 1) COUNT++;
    }
  }

  //end of loop
  FINISH_STATE_MESSAGE_LOOP;
  return 0;
}

/**
 * react accordingly following the conway's game of life
 * @return 0
 */
int react()
{
  /** react to neighbors:
   *
   * Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
   * Any live cell with two or three live neighbours lives on to the next generation.
   * Any live cell with more than three live neighbours dies, as if by overpopulation.
   * Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
   */

  // If alive (state == 1), live on if you have 2 or 3 neighbors; otherwise, die
  if (STATE == 1) {

      if (COUNT == 2 || COUNT == 3) {
          STATE = 1;
      }

      else {
          STATE = 0;
      }

  }

  // If dead (state == 0), become alive if you have exactly 3 neighbors (reproduction)
  else {

      if (COUNT == 3) {
          STATE = 1;
      }

  }

  return 0;
}
