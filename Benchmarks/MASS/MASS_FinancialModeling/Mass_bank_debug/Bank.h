/*=======================================================================
  Bank Class
  Author: Ian Dudder
=========================================================================
  The Bank Class is an agent in the Bail-in/Bail-out simulation that
  interacts with other classes in the following ways:
    - Provides loans to struggling Firm objects
    - Receives deposits from Workers
    - Fulfill loans from other Banks that are struggling
  Bank objects each keep track of their interest rates they offer on
  loans, their liquidity (i.e., assets; cash), and the debt accumulated
  from outstanding loans. Each Bank has a unique bank ID to identify 
  them.
=======================================================================*/

#ifndef BANK_H
#define BANK_H

#include <iostream>
#include "Agent.h"
#include "MASS_base.h"
#include "FinancialMarket.h"

/* Values passed into the bank in the init() function */
struct BankInitValues {
	double interestRate;
	double liquidity;
	bool makeLog;
};

class Bank : public Agent {
public:
	/*========================== Function IDs ===========================*/
	static const int init_ = 0;
	static const int timeStepInteraction_ = 1;
	static const int readRoundTransactions_ = 2;
	static const int checkBankruptcy_ = 3;
	static const int randomizeRate_ = 4;
	static const int reportRoundValues_ = 5;

	/*========================== Public Methods =========================*/

	/*--------------------------- Constructor ---------------------------*/
	Bank(void* argument) : Agent(argument) {}

	/*---------------------------- Call Method --------------------------*/
	virtual void *callMethod(int functionId, void* argument) {
		switch(functionId) {
			case init_ :
				return init(argument);

			case timeStepInteraction_ :
				return (void*)timeStepInteraction();

			case readRoundTransactions_ :
				return readRoundTransactions();

			case checkBankruptcy_ :
				return checkBankruptcy(argument);

			case randomizeRate_ :
				return randomizeRate(argument);

			case reportRoundValues_ :
				return reportRoundValues();

			default :
				return badFunctionId(functionId);
		}
		return NULL;
	}

private:
	/*======================= Call Method Helpers =======================*/

  /*------------------------------- init ------------------------------*/ 
  /** Initializes the Bank Object to specified values.
   *  @param initVals - An instance of the BankInitValues struct.       */
	void* init(void* initVals);

  /**------------------------ timeStepInteraction ----------------------/
   * Runs the Bank's sequence of actions in the simulation loop at each
   * round.                                                            */
	void* timeStepInteraction();

  /**----------------------- readRoundTransactions ---------------------/
   * Reads all payments made and loans given during this round from the 
   * Financial Market Place.                                           */
	void* readRoundTransactions();

  /*--------------------------- checkBankruptcy -----------------------*/
  /** Checks if this Bank has gone bankrupt based on its liquidity. 
   *  @param isBankrupt - a bool passed in by referece; set true if this 
   *         bank is bankrupt                                          */       
	void* checkBankruptcy(void* isBankrupt);

  /*---------------------------- randomizeRate ------------------------*/
  /** Randomizes the bank's interest rate for a new round, seeding it at
   *  the round's counter.           
   *  @param round - round counter in simulation loop                  */  
	void* randomizeRate(void* round);

  /*------------------------- reportRoundValues -----------------------*/
  /** Creates a new BankInfo struct (defined in FinancialMarket.h) to
   *  write to the Financial Market.                                   */ 
	void* reportRoundValues();

  /*---------------------------- badFunctionId ------------------------*/
  /** Handles bad calls to callMethod()                                */ 
	void* badFunctionId(int functionId);

	/*========================= Time Step Helpers =======================*/

  /*------------------------------ payLoans ---------------------------*/
  /** Attempts to pay back outstanding loans.                          */ 
	bool payLoans();

  /*------------------------- accumulateInterest ----------------------*/
  /** Increases the amount of outstanding loans due to gained interest.*/ 
	void accumulateInterest();

  /*------------------------------ getLoan ----------------------------*/
  /** Attempts to get a new loan from another Bank to prevent bankruptcy
  *   @param k - number of banks to approach for a loan                */ 
	bool getLoan(int k);

	/*=========================== Member Data ===========================*/
	double interestRate;            // Interest rate offered for loans.
	double liquidity;               // The Bank's assets/cash
	double totalDebt;               // Total debt from outstanding loans
	int bankId;                     // Unique bank identifier
	vector<Loan> debt;              // Outstanding loans
	ostringstream logText;          // Used to make MASS logs
};

#endif // BANK_H