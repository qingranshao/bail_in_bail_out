//
// Created by sarah on 8/10/20.
//

#ifndef LIFEPLACE_H
#define LIFEPLACE_H

#include "relogo/Patch.h"
#include "AgentPackage.h"

class LifePlace : public repast::relogo::Patch {

public:

    // Constructors / deconstructor

    LifePlace(const repast::AgentId& id, repast::relogo::Observer* obs):
        repast::relogo::Patch(id,obs), numNeighborsAlive(0) {

        // 50% of being initialized as being alive
        // TODO: check that randomness is correct
        _alive = rand() % 2 == 1;
    }

    LifePlace(const repast::AgentId id, repast::relogo::Observer* obs,
              const AgentPackage& package) :
              repast::relogo::Patch(id, obs), _alive(package.alive), numNeighborsAlive(0) {}  // constructor for
              // serialization

    virtual ~LifePlace() {}

    // State functions

    void checkNeighbors();   // check how many neighbors in this LifePlace's Moore's area are alive

    void react();   // change "alive" variable

    void printState(); // print alive state to screen

    // Getters and setters

    bool isAlive() const {
        return _alive;
    }

    void setAlive(bool alive) {
        _alive = alive;
    }

    // compile error without heading() ( ??? )
    double heading(float num) { return 0.0; }

private:
    bool _alive;

    int numNeighborsAlive;  // number of Moore area neighbors that were alive after last checkNeighbors() call
};


#endif //LIFEPLACE_H
