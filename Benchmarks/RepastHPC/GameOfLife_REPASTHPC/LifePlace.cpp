//
// Created by sarah on 8/10/20.
//

#include "LifePlace.h"

#define HIGH_TARGET 125
#define LOW_TARGET 97
// check how many neighbors are alive
void LifePlace::checkNeighbors() {
    numNeighborsAlive = 0;

    for (LifePlace *neighbor : neighbors<LifePlace>()) {
        if (neighbor->isAlive()) {
            numNeighborsAlive++;
        }
        if (pxCor() == 100 && pyCor() == 98) {
            cout << " 100 , 98's neighbor (" << neighbor->pxCor() << " , " << neighbor->pyCor() << ") is " <<
                 neighbor->_alive << endl;
        }
    }
}

// react to number of live neighbors according to Conway's Game of Life
void LifePlace::react() {
    if (numNeighborsAlive == 3) {
        _alive = true;
    } else if (!(numNeighborsAlive == 2 && _alive)) {
        _alive = false;
    }
}

void LifePlace::printState() {
    // local agents only within target range
    if (this->getId().startingRank() == this->getId().currentRank() &&
        this->pxCor() <= HIGH_TARGET && this->pyCor() <= HIGH_TARGET &&
        this->pxCor() >= LOW_TARGET && this->pyCor() >= LOW_TARGET) {
            cout << this->xCor() << " , " << this->yCor() << " is alive ? : " << _alive
                 << ", #neighbors = " << numNeighborsAlive << endl;
    }
}


