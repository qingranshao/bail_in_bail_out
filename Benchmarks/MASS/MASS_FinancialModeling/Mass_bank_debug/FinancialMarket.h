/*=======================================================================
	Financial Market Class
	Author: Ian Dudder
=========================================================================
	The FinancialMarket Class fosters all communication between agents in
	the Bail-in/Bail-out simulation. This class allows residing agents to
	read and write to it as a shared memory to communicate with one 
	another. This class will also use message-passing to send messages to
	other place elements to allow distant communication.
=======================================================================*/

#ifndef FINANCIALMARKET_H
#define FINANCIALMARKET_H

#include <iostream>
#include "Place.h"
#include "MASS_base.h"
#include <vector>
#include <map>
#include <unordered_map>
#include <set>

/* Helps firms calculate their workforce cost */
struct WorkerCost {
	double wages;
	int workerId;
};

/* Used to keep track of Banks' locations and rates */
struct BankInfo {
	double interestRate;
	double liquidity;
	int bankId;
	int xLoc;
	int yLoc;
};

/* Arguments to be passed into the selectFromKBanks function. */
struct BankSelectionArgs {
	double minAcceptableInterest;
	double amountNeeded;
	int k;
};

/* A record of a loan from a bank */
struct Loan {
	double amount;  
	double interestRate;
	int loaningBankId;
};

class FinancialMarket : public Place {
public:
	/*========================== Function IDs ===========================*/
	// Invoked by callAll
	static const int init_ = 0;
	// (1-3) Help broadcasts current round's bank interest rates
	static const int prepBankInfoExchange_ = 1;     // Prepares outmessage
	static const int exchangeBankInfo_ = 2;         // Passes bank info
	static const int updateBankRegistry_ = 3;       // updates bankRegistry
	// (4-8) Help send out outgoing transactions to banks
	static const int prepForIncomingTransactions_ = 4;
	static const int exchangeOutgoingTransactions_ = 5;
	static const int manageIncomingTransactions_ = 6;
  static const int resetRoundValues_ = 7;
  static const int seedRound_ = 8;
	
	// Invoked by residing Agents
  // (10-14) Used to prepare all places and workers for the simulation
	static const int addWorker_ = 9;               // Add Workers wages
	static const int addFirm_ = 10;                 // sets residingFirm info
	static const int addBank_ = 11;                 // sets residingBank info
	static const int calculateWorkforceCost_ = 12;  // Help firms
	static const int updateBankInfo_ = 13;          // Updates bankRegistry of single changes
	static const int selectFromKBanks_ = 14;        // Selects a bank from k banks
	static const int addBankTransaction_ = 15;    
	static const int getIncomingTransactionAmt_ = 16;  
	static const int getNumFirms_ = 17;

	/*========================== Public Methods =========================*/
		
	/*--------------------------- Constructors --------------------------*/
	FinancialMarket(void* arg) : Place(arg) { /**/}

	/*---------------------------- Call Method --------------------------*/
	virtual void *callMethod(int functionId, void* argument) {
		switch(functionId) {
			case init_ :
				return init(argument);

			case addWorker_ :
				return addWorker(argument);

			case calculateWorkforceCost_ :
				return calculateWorkforceCost();

			case addFirm_ :
				return addFirm(argument);
			
			case addBank_ :
				return addBank(argument);

			case updateBankInfo_ :
				return updateBankInfo(argument);

			case prepBankInfoExchange_ :
				return prepBankInfoExchange();

			case exchangeBankInfo_ :
				return exchangeBankInfo();

			case updateBankRegistry_ :
				return updateBankRegistry();

			case selectFromKBanks_ :
				return selectFromKBanks(argument);

			case addBankTransaction_ :
				return addBankTransaction(argument);

			case prepForIncomingTransactions_ :
				return prepForIncomingTransactions();

			case exchangeOutgoingTransactions_ :
				return exchangeOutgoingTransactions(argument);

			case manageIncomingTransactions_ :
				return manageIncomingTransactions();

			case getIncomingTransactionAmt_ :
				return getIncomingTransactionAmt();

			case getNumFirms_ :
				return getNumFirms();

      case resetRoundValues_ :
        return resetRoundValues();

      case seedRound_ :
        return seedRound(argument);
		}
		return nullptr;
	}

private:
	/*======================= Call Method Helpers =======================*/

	/*------------------------------ init -------------------------------*/
	/** Initializes member data for the Financial Market object.
		*  @param nAgents - a pair<int,int> where the first element is the 
    *         number of banks, and the second is the number of firms.  */
	void* init(void* nAgents);

	/*----------------------------- addWorker ---------------------------*/
	/** Allows Worker agents to report their wage for the Firm to read.
	 *  @param workerInfo - a WorkerCost object                          */
	void* addWorker(void* workerInfo);

	/*------------------------ calculateWorkforceCost -------------------*/
	/** Sums the wages of all residing workers for the Firm to read.     */
	void* calculateWorkforceCost();

	/*------------------------------ addFirm ----------------------------*/
	/** Sets the residingFirm data member.															 */
	void* addFirm(void* firmArg);

	/*------------------------------ addBank ----------------------------*/
	/** Sets the residingBank data member and updates the bankregistry.  
	 *  @param bankInfo - a BankInfo struct to store info on Banks.      */
	void* addBank(void* bankInfo); 

	/*--------------------------- updateBankInfo ------------------------*/
	/** Updates the bankregistry with the given argument.								 
	 *  @param updatedInfo - a BankInfo struct reflecting updated changes
	 *         to the bank's information.                                */
	void* updateBankInfo(void* updatedInfo);

	/*------------------------- prepBankInfoExchange --------------------*/
	/** Prepares Place elements to pass their BankInfo to update each 
	 *  element's bankRegistry.                                          */
	void* prepBankInfoExchange();

	/*--------------------------- exchangeBankInfo ----------------------*/
	/** Passes the BankInfo object set up by each Bank in the simulation.*/
	void* exchangeBankInfo();

	/*--------------------------- updateBankRegistry --------------------*/
	/** Updates the bankRegistry based on values received from other Place
	 *  elements.                                             					 */
	void* updateBankRegistry();

	/*---------------------------- selectFromKBanks ---------------------*/
	/** Helps Agents find Banks to loan money to them. Randomly approaches
	 *  k banks and returns the bankId of the bank with the lowest interest
	 *  rate that can affort to give the loan.                           
	 *  @param arg - a BankSelectionArgs instance                        */
	void* selectFromKBanks(void* arg);

	/*------------------------- addBankTransaction ----------------------*/
	/** Adds a new payment or loan to the outgoingTransactions data member
	 *  to send to a Bank on another place.															 
	 *  @param transactionInfo - a pair<int,double>. The int is the bankId,
	 *         the double is the amount of the transaction.              */
	void* addBankTransaction(void* transactionInfo);

	/*--------------------- prepForIncomingTransaction ------------------*/
	/** Prepares places to receive transactions sent by other places.    */
	void* prepForIncomingTransactions();

	/*--------------------- exchangeOutgoingTransactions ----------------*/
	/** Receives a bankId from a remote place and returns the sum of all
	 *  transactions made on the place for that bank.                   
	 *  @param givenBankId - the ID of the bank associated with the 
	 *         transaction 																							 */
	void* exchangeOutgoingTransactions(void* givenBankId);

	/*--------------------- manageIncomingTransactions ------------------*/
	/** Totals up all transactions received from other places and stores it
	 *  for the residing bank to read in.																 */
	void* manageIncomingTransactions();

	/*--------------------- getIncomingTransactionAmt ------------------*/
	/** Returns the sum of payments/loans received.                     */
	void* getIncomingTransactionAmt();

	/*---------------------------- getNumFirms --------------------------*/
	/** Returns the number of firms in the simulation.                   */
	void* getNumFirms();

  /*------------------------- resetRoundValues ------------------------*/
	/** Resets round-specific values to prepare for another round.       */
  void* resetRoundValues();

  /*---------------------------- seedRound ----------------------------*/
	/** Seeds the round for testable output.                             */
  void* seedRound(void* seed);


  /*====================== Private Helper Methods =====================*/

  /*------------------------- uniqueAddNeighbor -----------------------*/
	/** Checks if a neighbor has already been added and adds it if it was 
  *   not. Returns true if the neighbor is added during this call and 
  *   returns false if the neighbor was already added previously.      
  *   @param x - the neighbor's x-location (column)
  *   @param y - the neighbor's y-location (row)                       */
  bool uniqueAddNeighbor(const int x, const int y);


	/*=========================== Member Data ===========================*/
	double incomingTransactions; /* Sum of payments/loans for local bank */

	int numBanks;       /* Number of banks in the simulation */

  int numFirms;       /* Number of firms in the simulation */

	int residingBank;   /* ID of Bank Agent that (may) reside here */

	int residingFirm;   /* ID of Firm Agent that (may) reside here */

	unordered_map<int /*bankId*/, double /*Amount*/> outgoingTransactions;

	unordered_map<int, double> localWorkerWages; /* Wages of local workers */

	unordered_map<int, BankInfo> bankRegistry;   /* Stores information on Banks */

	ostringstream logText;  /* Used to send messages to MASS log */
};


#endif //FINANCIALMARKET_H