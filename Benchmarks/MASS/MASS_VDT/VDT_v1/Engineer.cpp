#include "Engineer.h"


extern "C" Agent* instantiate(void* argument) {
  return new Engineer(argument);
}


extern "C" void destroy(Agent* engineerObj) {
  delete engineerObj;
}


//---------------------------------- work ------------------------------------
/** Has the Engineer Agent work for an hour on a Task. 
 *  @param dayAndHour - a pair<int,int> object with the first item as the day
 *         and the second item as the hour.                                  
 */
void* Engineer::work(void* dayAndHour) {
  // 1) Get the current hour and day from the Team Place
  // 2) Check if there is a collaborative task to attend.
  // 3) If no collaborative tasks, check on our current production task.
  //    a) If there is no production task, get one.
  //    b) If there are none left for this project, try to move on.
  // 4) Work on the current task (reduce hours by 1).
  // 5) Check if the task is complete. If so, return it 
  return nullptr;
}

