#include "Worker.h"

using namespace std;


extern "C" Agent* instantiate(void* argument) {
  return new Worker(argument);
}

extern "C" void destroy(Agent* obj) {
  delete obj;
}


/*---------------------------- Destructors --------------------------*/
Worker::~Worker() {
  // No dynamic memory to delete
}

/*-------------------------------- Map ------------------------------*/
int Worker::map(int initPopulation, vector<int> size, vector<int> index,
              Place* curPlace) {
  
  logText.str("");
  logText << endl << "Mapping Worker to "
          << curPlace->index[0] << ", " << curPlace->index[1] << endl;

  void* dummyArg;
  // Check if the current place has a firm. If not, don't map a worker
  int numFirms = *(int*) curPlace->callMethod(FinancialMarket::getNumFirms_, dummyArg);

  // compute the global linear index
  int linearIndex = 0;
  for (int i = 0; i < int(index.size()); i++) {
    if (index[i] >= 0 && size[i] > 0 && index[i] < size[i]) {
      linearIndex = linearIndex * size[i];
      linearIndex += index[i];
    }
  }
  if (linearIndex >= numFirms) {
    logText << "  - No Firm Resides here. No Workers mapped." << endl;
    MASS_base::log(logText.str());
    return 0; // No Firm resides here. Abandon mapping.
  }

  // compute the total # places
  int placeTotal = 1;
  for (int x = 0; x < int(size.size()); x++) placeTotal *= size[x];

  logText << "  - Place total: " << placeTotal << endl;
  logText << "  - Linear Index: " << linearIndex << endl;
  logText << "  - Init population: " << initPopulation << endl;

  // compute #agents per place a.k.a. colonists
  int colonists = initPopulation / numFirms;
  int remainders = initPopulation % numFirms;
  if (linearIndex < remainders) colonists++;  // add a remainder
  
  logText << "  - Colonists " << colonists << endl;
  logText << "  - Remainders " << remainders << endl;

  MASS_base::log(logText.str());
  return colonists;
}


/*------------------------------ init -------------------------------*/
/** Initializes member data for the Worker object and writes the wages
 *  to the Financial Market it resides on for its Firm to read.
 *  @param initVals - a WorkerInitValues struct object               */
void* Worker::init(void* initVals) {
  WorkerInitValues initArgs = *(WorkerInitValues*)initVals;
  // Set up worker's member data
  wages = initArgs.defaultWage / (initArgs.wageDivisor * ((float)rand()/RAND_MAX + 0.5));
  capital = 0;
  consumptionBudget = 0.3;
  workerId = agentId;

  // Write wages to Financial Market Place
  WorkerCost wc;
  wc.workerId = workerId;
  wc.wages = wages;
  void* workerInfo = &wc;
  place->callMethod(FinancialMarket::addWorker_, workerInfo);

  // Select a random bank
  BankSelectionArgs selArgs;
  selArgs.minAcceptableInterest = INT_MAX;
  selArgs.amountNeeded = -1;
  selArgs.k = 1;
  BankInfo selectedBank = *(BankInfo*)place->callMethod
      (FinancialMarket::selectFromKBanks_, &selArgs);
  myBank = selectedBank.bankId;

  // Make log of creation
  if (initArgs.makeLog) {
      logText.str("");
      logText << endl;
      logText << "Creating Worker " << agentId << " at " << index[0] << ", " << index [1] << endl
              << "   Has wage " << wages << endl
              << "   Has Bank " << myBank << endl << endl;
      MASS_base::log(logText.str());
  }
  return nullptr;
}


/*----------------------- timeStepInteraction -----------------------*/
/** Runs the Workers's sequence of actions during a round in the 
 *  simulation loop.                                                 */
bool* Worker::timeStepInteraction() {
  logText.str("");
  logText << "Worker " << workerId << " now acting..." << endl;
  receiveWages();
  consume();
  depositFunds();

  MASS_base::log(logText.str());
  return nullptr;
}


/*-------------------------- receiveWages ---------------------------*/
/** Increase capital based on income.                                */
void Worker::receiveWages() {
  logText << "  - Receiving wage $" << wages << endl;
  capital += wages;
}


/*----------------------------- consume -----------------------------*/
/** Spend wages.                                                     */
void Worker::consume() {
  double percentOfWage = (float) ( (float)rand() / RAND_MAX);
  if (percentOfWage < 0.4) {
    percentOfWage = 0.4;
  }
  consumptionBudget = wages * percentOfWage;
  logText << "  - Spending $" << consumptionBudget << endl;
  MASS_base::log(logText.str());
}

/*--------------------------- depositFunds --------------------------*/
/** Sends a deposit to the Worker's Bank.                            */
bool Worker::depositFunds() {
  logText << "  - Making Deposit $" << wages - consumptionBudget << " to bank " << myBank << endl;
  pair<int,double> deposit;
  deposit.first = myBank;
  deposit.second = wages - consumptionBudget; // Positive value
  place->callMethod(FinancialMarket::addBankTransaction_, (void*)&deposit);
  return false;
}

// End Worker.cpp