#include "Vehicle.h"
#include "MASS_base.h"
#include <limits.h>
#include <iostream>
#include <string>
#include <random>

extern "C" Agent* instantiate( void *argument ) {
  return new Vehicle( argument );
}

extern "C" void destroy( Agent *object ) {
  delete object;
}

// set up cardinal direction's relative indexes
int Vehicle::cardinals[8][2] = { { 0,1 },{ 1,0 },{ 0,-1 },{ -1,0 },{ -1,1 },{ 1,1 },{ -1,-1 },{ 1,-1 } };

// initialize the Vehicle
void *Vehicle::initialize(void *argument) {

   totalGridWidth = 200;
   totalGridSize = totalGridWidth*totalGridWidth;

   
   ostringstream convert;
   amountOfMoney = 1;
   streetsTraversed = STARTING_ENERGY;

   int xDestination = rand() % totalGridWidth;
   int yDestination = 0;
   destination = xDestination;
   
   MASS_base::log(convert.str());
	return NULL;
}


//displays agent status
void *Vehicle::displayAgentStatus(void *argument) {
ostringstream convert;

int idNum = place->index[0];
convert << "car" << agentId << "'s source = " << idNum << " and destination is: " << destination;
sourceTarget[0] = destination;
MASS_base::log(convert.str());
return NULL;
}


// updateAgentStatus
// update agent for current time period
//get shortest path from source to destination for agent.
void *Vehicle::updateAgentStatus(void *argument) {
   int *pathWithSize = new int[INT_MAX];
   pathWithSize = (int *)place->callMethod(Street::getShortestPath_, (void*)destination);

   for (int i = 0; i <= pathWithSize[0]; i++) {
      shortestPath.push_back(pathWithSize[i + 1]);
   }
   return NULL;
}



// moveAgent
void *Vehicle::moveAgent(void *argument) {

   int idNum;
   int x;
   int y;
   int totalAgents = 7200;
   int totalPlaces = 200;

   int numAgents = place->agents.size();
   if (numAgents <= (totalAgents/totalPlaces)*2) { //(#agents/#places) *2
      if (streetsTraversed < shortestPath.size() - 1) {
         // move agent to new location
         vector<int> dest;
         idNum = shortestPath.at(streetsTraversed);
           
         x = idNum;

         if (idNum < 200) {
            dest.push_back(x);
            dest.push_back(0);
         }

         migrate(dest);

         ostringstream convert;
         convert << "car" << agentId << " has moved to: " << idNum << endl;
         MASS_base::log(convert.str());
      }
   }
   return NULL;
}

// killAgent
// kill this agent
void *Vehicle::killAgent(void *argument) {
   int currLoc = shortestPath.at(streetsTraversed);
   if (currLoc == destination) {
      kill();
   }
   streetsTraversed++;
   return NULL;
}


//displays agent status
//7200 agents 200 places
void *Vehicle::updateCarInitialState(void *argument) {

   if ((int)agentId < 216) {
      destination = rand() % 12;
   }
   else if ((int)agentId < 1152) { 
      destination = rand() % 54 + 12;
   }
   else if ((int)agentId < 6048) { 
      destination = rand() % 68 + 66;
   }
   else if ((int)agentId < 6984) {
      destination = rand() % 54 + 134;
   }
   else {
      destination = rand() % 12 + 188;
   }


   //flip this so its reversed
   vector<int> dest;
   int x;
   if ((int)agentId < 2448) { 
      x = rand() % 34;  
   }
   else if ((int)agentId < 3384) { 
      x = rand() % 54 + 34; 
   }
   else if ((int)agentId < 3816) { 
      x = rand() % 88 + 24; 
   }
   else if ((int)agentId < 4752) {
      x = rand() % 54 + 112;  
   }
   else {
      x = rand() % 34 + 166; 
   }

   dest.push_back(x);
   dest.push_back(0);
   migrate(dest);

   return NULL;
}