#include "Bank.h"

using namespace std;

extern "C" Agent* instantiate(void* argument) {
  return new Bank(argument);
}

extern "C" void destroy(Agent* obj) {
  delete obj;
}

/*------------------------------- init ------------------------------*/ 
void* Bank::init(void* initVals) {
  // Set member data
	BankInitValues initArgs = *(BankInitValues*)initVals;
	interestRate = initArgs.interestRate;
	liquidity = initArgs.liquidity;
	totalDebt = 0;
	bankId = agentId;
  	
	// Pass info to Place to write to BankRegistry
	BankInfo myInfo;
	myInfo.interestRate = interestRate;
	myInfo.liquidity = liquidity;
	myInfo.bankId = bankId;
	myInfo.xLoc = index[0];
	myInfo.yLoc = index[1];
	place->callMethod(FinancialMarket::addBank_, &myInfo);
	return nullptr;
}

/**------------------------ timeStepInteraction ---------------------*/
void* Bank::timeStepInteraction() {
	payLoans();           // 1) Pay outstanding loans to other banks
	accumulateInterest(); // 2) Accumulate interest on outstanding loans
	if (liquidity < 0)    // 3) Get a loan if needed
    getLoan(3);
	return nullptr;
}

/*------------------------------ payLoans ---------------------------*/
bool Bank::payLoans() {
	bool couldPay = true;
	for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
		double amtToPay  = debt[curLoan].amount / 2;
		if (liquidity > amtToPay) {
			debt[curLoan].amount -= amtToPay;
			liquidity -= amtToPay;
			totalDebt -= amtToPay;
      // Pass payment to the Financial Market to forward to the bank
			pair<int,double> payment;
			payment.first = debt[curLoan].loaningBankId;
			payment.second = amtToPay; // Positive value for payment
			place->callMethod(FinancialMarket::addBankTransaction_, 
          (void*)&payment);
		}
		else {
			liquidity = -1;
			couldPay = false;
			break;
		}
	}
	return couldPay;
}

/*------------------------- accumulateInterest ----------------------*/
void Bank::accumulateInterest() {
	for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
		double increase = debt[curLoan].amount * debt[curLoan].interestRate;
		debt[curLoan].amount += increase;
		totalDebt += increase;
	}
}

/*------------------------------ getLoan ----------------------------*/
bool Bank::getLoan(int k) {
	BankSelectionArgs selArgs;
	selArgs.minAcceptableInterest = 2134.0;
	selArgs.amountNeeded = liquidity * -1;
	selArgs.k = k;
	BankInfo selectedBank = *(BankInfo*)place->callMethod
      (FinancialMarket::selectFromKBanks_, &selArgs);
	if (selectedBank.bankId != -1) {
		// Add loan to debt
		Loan newLoan;
		newLoan.amount = liquidity * -1; // Save as a positive value
		newLoan.interestRate = selectedBank.interestRate;
		newLoan.loaningBankId = selectedBank.bankId;
		debt.push_back(newLoan);
		totalDebt += newLoan.amount;
		liquidity += newLoan.amount;
    // Send Loan to the Financial Market
		pair<int,double> loanReceipt;
		loanReceipt.first = selectedBank.bankId;
		loanReceipt.second = newLoan.amount * -1; // Send to bank as negative
		place->callMethod(FinancialMarket::addBankTransaction_, 
        (void*)&loanReceipt);
		return true;
	}
	else {
		return false;
	}
}

/*---------------------------- randomizeRate ------------------------*/
void* Bank::randomizeRate(void* round) {
	interestRate = (float) rand() / RAND_MAX;
  return nullptr;
}

/*------------------------- reportRoundValues -----------------------*/
void* Bank::reportRoundValues() {
	BankInfo myInfo;
	myInfo.interestRate = interestRate;
	myInfo.liquidity = liquidity;
	myInfo.bankId = bankId;
	myInfo.xLoc = index[0];
	myInfo.yLoc = index[1];
	place->callMethod(FinancialMarket::updateBankInfo_, &myInfo);
	return nullptr;
}

/**----------------------- readRoundTransactions --------------------*/
void* Bank::readRoundTransactions() {
	void* dummyArg;
	double transactionAmt = *(double*)place->callMethod
      (FinancialMarket::getIncomingTransactionAmt_, dummyArg);
	liquidity += transactionAmt;
	return nullptr;
}

/*--------------------------- checkBankruptcy -----------------------*/
void* Bank::checkBankruptcy(void* isBankrupt) {
	if ((liquidity + totalDebt < 0)) {
		*(bool*)isBankrupt = true;
    logText.str("");
		logText << "Bank " << bankId << " BANKRUPT!!!" << endl;
    MASS_base::log(logText.str());
	}
	return nullptr;
}

/*---------------------------- badFunctionId ------------------------*/
void* Bank::badFunctionId(int functionId) {
	logText.str("");
	logText << "Error: agent " << agentId << " ran invalid function: " 
					<< functionId << endl;
	MASS_base::log(logText.str());
	return nullptr;
}

// End Bank.cpp