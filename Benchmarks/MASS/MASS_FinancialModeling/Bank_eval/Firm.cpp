#include "Firm.h"

using namespace std;

extern "C" Agent* instantiate(void* argument) {
  return new Firm(argument);
}

extern "C" void destroy(Agent* obj) {
  delete obj;
}

/*------------------------------ init -------------------------------*/
void* Firm::init(void* initVals) {
  FirmInitValues initArgs = *(FirmInitValues*) initVals;
  productionCost = resetVals.productionCost = initArgs.productionCost;
  liquidity = resetVals.liquidity = initArgs.liquidity;
  profit = 0;
  firmId = agentId;
  owner = new Owner(firmId, 0);
  place->callMethod(FinancialMarket::addFirm_, &firmId);
  return nullptr;
}

/*--------------------- calculateWorkforceCost ----------------------*/
void* Firm::calculateWorkforceCost(void* makeLog) {
  void* dummyArg;
  workforceCost = *(double*)place->callMethod(FinancialMarket::calculateWorkforceCost_, dummyArg);
  return nullptr;
}

/*---------------------------- badFunctionId ------------------------*/
void* Firm::badFunctionId(int functionId) {
  logText.str("");
  logText << "Error: agent " << agentId << " ran invalid function: " 
          << functionId << endl;
  MASS_base::log(logText.str());
  return nullptr;
}

/*----------------------- timeStepInteraction -----------------------*/
void* Firm::timeStepInteraction() {
  accumulateInterest();   // 1) Calculate Interest on outstanding loans
  calculateProducitonCosts(); // 2) Calculate production costs
  calculateProfits();     // 3) Calculate Profits
  payWorkforce();         // 4) Pay Worker Wages
  calculateLiquidity();   // 5) Calculate liquidity
  payBank();              // 6) Pay Banks for outstanding loans
  bool costsCovered = (liquidity >= 0);
  if (!costsCovered) {
    costsCovered = getOwnerLoan(); // 7a) Ask Owner for loan
    if (!costsCovered && debt.size() <= 0) {
      costsCovered = requestBankLoan(3);
    }
  }
  if (liquidity < 0)
    resetFirm();
  return (void*)&costsCovered;
}

/*------------------------- accumulateInterest ----------------------*/
void Firm::accumulateInterest() {
  for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
    debt[curLoan].amount = debt[curLoan].amount * (1.0 + debt[curLoan].interestRate);
  }
}

/*--------------------- calculateProductionCosts --------------------*/
double Firm::calculateProducitonCosts() {
  double modifier = (float) ( (float)rand() / RAND_MAX + 0.5);
  productionCost *= modifier;
  return productionCost;
}

/*------------------------- calculateProfits ------------------------*/
double Firm::calculateProfits() {
  double x = (float) ( (float)rand() / RAND_MAX + 0.5);
  profit = (productionCost * x) - productionCost;
  return profit;
}

/*--------------------------- payWorkforce --------------------------*/
bool Firm::payWorkforce() {
  bool paidOwner = false;
  profit -= workforceCost;    // Pay Workers
  double dividends;           // Owner's cut        
  if (profit > 0) {
    dividends = profit * 0.2;
    profit -= dividends;
  }
  else {
    dividends = 0;
  }
  owner->setCapital(owner->getCapital() + dividends);
  return paidOwner;
}

/*------------------------ calculateLiquidity -----------------------*/
double Firm::calculateLiquidity() {
    liquidity += profit;
    return liquidity;
}

/*------------------------------ payBank ----------------------------*/
bool Firm::payBank() {
  bool couldPay = true;
  for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
    double amtToPay = debt[curLoan].amount / 2;
    debt[curLoan].amount -= amtToPay;
    liquidity -= amtToPay;
    pair<int,double> payment;
    payment.first = debt[curLoan].loaningBankId;
    payment.second = amtToPay; // Positive value
    place->callMethod(FinancialMarket::addBankTransaction_, (void*)&payment);
  }
  return couldPay;
}

/*---------------------------- getOwnerLoan -------------------------*/
bool Firm::getOwnerLoan() {
  if (owner->getCapital() > liquidity * -1) {
    double amount = liquidity * -1;
    owner->setCapital(owner->getCapital() - amount);
    liquidity += amount;
    return true;
  }
  return false;
}

/*-------------------------- requestBankLoan ------------------------*/
bool Firm::requestBankLoan(int k) {
  // Select the bank with the lowest interest rate
  BankSelectionArgs selArgs;
  selArgs.minAcceptableInterest = 2134.0;
  selArgs.amountNeeded = liquidity * -1;
  selArgs.k = k;
  BankInfo selectedBank = *(BankInfo*)place->callMethod
      (FinancialMarket::selectFromKBanks_, &selArgs);

  if (selectedBank.bankId != -1) {
    // Send a loan acceptance through a bank transaction
    Loan newLoan;
    newLoan.amount = liquidity * -1; // Positive value
    newLoan.interestRate = selectedBank.interestRate;
    newLoan.loaningBankId = selectedBank.bankId;
    debt.push_back(newLoan);
    liquidity += newLoan.amount;

    /*Add loan information to Financial Market's Bank Transactions*/
    pair<int,double> loanReceipt;
    loanReceipt.first = selectedBank.bankId;
    loanReceipt.second = newLoan.amount * -1; // Negative value
    place->callMethod(FinancialMarket::addBankTransaction_, (void*)&loanReceipt);
    return true;
  }
  else {
    return false;
  }
}

/*----------------------------- resetFirm ---------------------------*/
void Firm::resetFirm() {
  productionCost = resetVals.productionCost;
  liquidity = resetVals.liquidity;
  profit = 0;
  owner->setCapital(liquidity * ((float) rand() / RAND_MAX + 0.5));
  debt.clear();
}

// End Firm.cpp