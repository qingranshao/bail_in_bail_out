#include "ProjectManager.h"

void ProjectManager::addTeam(int id, int totalPeople, int avgWage, int avgIntel, Graph::Process* current) {
  // Initializing leader
  // SubFunctionLeader(int id, int wageNum, int intel, int resources);
  SubFunctionLeader* temp = new SubFunctionLeader(id, 40, avgIntel, 50000);
  totalResources -= 50000;

  // Set Leader's Team
  temp->team = new SubFunctionLeader::Team(totalPeople, avgIntel, avgWage);

  // Add Leader's stats to Team stats
  temp->team->totalIntel += temp->knowledgeNum;
  temp->team->totalWages += temp->wage;

  // Set Team's Location
  temp->currentProcess = current;
  if (current != nullptr) {
    current->currentTeam = id;
  }

  // Adding Team to PM's control
  teamLeaders.push_back(temp);
}

// Runs active teams
void ProjectManager::runTeams() {
  // Run individual teams (i.e. via their leaders)
  for (int i = 0; i<teamLeaders.size(); i++) {
    // Run team and recieve response
    Person::Message* msg;
    msg = (*teamLeaders[i]).runTeam();

    // Delegate exceptions
    if (msg != nullptr) {
      inQueue.push_back(msg);
    }

    // Team has finished processing
    if (teamLeaders[i]->currentProcess->isCompleted) {
      teamPostProcessing(teamLeaders[i]);
    }

  }
  // Process exceptions
  processExceptions(3);
}


// When called assumed that team1 is the smaller id
void ProjectManager::mergeTeams(SubFunctionLeader* srcLeader, SubFunctionLeader* destLeader) {
  // Updating team stats
  destLeader->totalTeamResources += srcLeader->totalTeamResources;
  destLeader->team->totalIntel += srcLeader->team->totalIntel - srcLeader->knowledgeNum;
  destLeader->team->totalWages += srcLeader->team->totalWages - srcLeader->wage;

  // Migrate workers from team2 to team1
  std::vector<SubFunctionMember*>::iterator it;
  destLeader->team->members.insert(it + destLeader->team->members.size() - 1, srcLeader->team->members.begin(), srcLeader->team->members.end());

  // Removing remnants of team
  //std::cout << "Firing Team Leader: " << srcLeader->teamId << "\n";
  freedTeamLeaders.push_back(srcLeader->teamId);
  delete teamLeaders[srcLeader->teamId];
  teamLeaders[freedTeamLeaders.back()] = nullptr;
}


void ProjectManager::teamPostProcessing(SubFunctionLeader* leader) {
  // Used for brevity
  Graph::Process* tempProcess = leader->currentProcess;
  SubFunctionLeader* tempLeader = leader;

  // Merging
  if (tempProcess->next.size() == 1 && tempProcess->currentTeam != -1) {
    mergeTeams(leader, teamLeaders[tempProcess->currentTeam]);
    return;
  }

  // Forking
  for (int p_id = 0; p_id < tempProcess->next.size(); p_id++) {

    // Next process has no current team, so move current team
    if (tempProcess->next[p_id]->currentTeam == -1) {
      // Updating the team's currentProcess
      tempLeader->currentProcess = tempProcess->next[p_id];
      // Notifying the currentProcess of team
      tempLeader->currentProcess->currentTeam = tempLeader->teamId;
      // Updating parental requirements of currentProcess
      tempLeader->currentProcess->completedParents++;
    } // Moved a team onto an unoccupied process

      // Create new team if necessary
    if (p_id < tempProcess->next.size() - 1) {
      // Looking for available teams
      if (!availableTeamLeaders.empty()) {
        tempLeader = teamLeaders[ availableTeamLeaders.back() ];
        availableTeamLeaders.pop_back();
      }
      else {
        int newTeamId = teamLeaders.size();

        // Recycle available teamIds
        if (!freedTeamLeaders.empty()) {
          newTeamId = freedTeamLeaders.back();
          freedTeamLeaders.pop_back();
        }

        //  addTeam(int id, int totalPeople, int avgWage, int avgIntel, Graph::Process* current);
        addTeam(teamLeaders.size(), 50, 20, 30, tempProcess->next[p_id]);
      }
    }
  } // Finished occupying all next processes

  // No process found for team, make available
  if (tempLeader->currentProcess == nullptr) {
    availableTeamLeaders.push_back(tempLeader->teamId);
  }

  // Cleanly handle temp pointer
  tempProcess = nullptr;
  tempLeader = nullptr;
}

// Processes n-many Exceptions
void ProjectManager::processExceptions(int nExceptions) {
  // Error Handling
  if (inQueue.empty()) {
    return;
  }

  for (int i = 0; i < nExceptions || i < inQueue.size(); i++) {
    // Obtain message
    Person::Message *msg = inQueue.back();
    inQueue.pop_back();

    // Respond to Resource Acquisition
    if (msg->resourcesNumRequest != 0) {
      // Determining Funds as in Red/Black
      if (totalResources - msg->resourcesNumRequest < 0) {
        totalResources += 2 * msg->resourcesNumRequest;
        loans += 2 * msg->resourcesNumRequest;
      }
      totalResources -= msg->resourcesNumRequest;
      teamLeaders[msg->teamNum]->totalTeamResources += msg->resourcesNumRequest;
    }

    // Respond to Personale Acquisition
    if (msg->workerNumRequest != 0) {
      // Init members
      for (int i = 1; i < msg->workerNumRequest; i++) {
        // SubFunctionMember(int wageNum, int avgIntel)
        SubFunctionMember* temp = new SubFunctionMember(msg->avgWage, msg->avgIntel);
        teamLeaders[msg->teamNum]->team->members.push_back(temp);
        // Updating totalIntel, and totalWages
        teamLeaders[msg->teamNum]->team->totalIntel += temp->knowledgeNum;
        teamLeaders[msg->teamNum]->team->totalWages += temp->wage;
      }
    }
  }
}