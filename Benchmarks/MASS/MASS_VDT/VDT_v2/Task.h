/*==============================================================================
  Task Class
  Author: Ian Dudder
================================================================================
  A number of Task objects will form a Project in the VDT simulation. There are
  two types of tasks: Production (type 0) and Collaborative (type 1). 

  Production Tasks flow through the task hierarchy and have a predefined number
  of hours required from each Engineer type. When a Production Task is selected
  by an Engineer Agent, there is an E percent chance that the task experiences
  an exception and requires the Engineer to reassign it to their supervisor for
  some number of hours. 

  Collaborative Tasks simulate communication and meetings between team members,
  and are scheduled to occur a set number of times during the day. When an 
  Engineer is assigned to a Collaborative Task, the agent will be unable to
  work on any other tasks.
==============================================================================*/

#ifndef TASK_H
#define TASK_H

#include <iostream>
#include <vector>

using namespace std;

class Task {
public:
  /*============================== Public Methods ============================*/
  Task();

  Task(int type, int taskId, int priority);

  bool makeProgress(int engType);



  /*================================= Getters ===============================*/
  int getType() const;

  int getId() const;

  int getPriority() const;

  int getTotalHours() const;

  int getCollabDay() const;

  /*================================= Setters ===============================*/

private:
  /*================================ Member Data =============================*/
  int type;              // Type of Task (0 for Production, 1 for Collaborative)
  int priority;          // Priority of task
  int totalHours;        // Total number of hours required from all engineers
  int taskId;            // Unique Identifier for the task
  int projectId;         // Tells which project this task is a part of
  int collabStartDay;    // Start time for collaborative task
  vector<int> hours;     // Number of hours required from each engineer type
};

#endif //TASK_H