#include "Agent.h"

int Agent::sendMsgTo(int agent, int degree) {
  // Increment count
  countPerDegree.reserve(degree);
  countPerDegree[degree-1]++;

  // floating point operations
  int retVal = 0;
  for (int i=0; i < 5; i++) {
    retVal += i*10;
  }
  return retVal;
}

// Stats on recieve is unnecessary for the benchmark test
void Agent::recieveMsgFrom(int agent, int degree, int msg) {
  return;
}
