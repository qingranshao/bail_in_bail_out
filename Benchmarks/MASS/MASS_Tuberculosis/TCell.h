//
// Created by sarah
//

#ifndef TCELL_H
#define TCELL_H

#include <iostream>
#include <vector>
#include <sstream>
#include "Agent.h"
#include "MASS_base.h"
#include "TBConstants.h"

class TCell : public Agent {

public:
    TCell(void *argument) : Agent(argument) {}

    // state functions
    static const int init_ = 0;
    static const int requestMove_ = 1;
    static const int move_ = 2;
    static const int arrival_ = 3;

    static const int spawn_ = 4;
    static const int newTCellInit_ = 5;

    // for migration
    static const int addMigrationData_ = 6;
    static const int modifyMigrationData_ = 7;

    // for testing
    static const int printName_ = 8;
    static const int printLocation_ = 9;
    static const int writeTCellToXML_ = 10;

    virtual void *callMethod(int functionId, void *argument) {
        switch (functionId) {
            // only called on spawners
            case init_ :
                return init();
            case requestMove_ :
                return requestMove();
            case move_ :
                return move();
            case arrival_:
                return arrival();

            case spawn_ : return spawnTCell(); // for spawners only
            case newTCellInit_ : return newTCellInit(); // for newly spawned tCells

                // for migration
            case addMigrationData_ : return addMigrationData();

            case modifyMigrationData_ : return modifyMigrationData();

                // for testing
            case printName_:
                return printName();
            case printLocation_:
                return printLocation();
            case writeTCellToXML_ :
                return writeTCellToXML();
            default:
                return NULL;
        };

    }

private:

    // only spawnPtNum TCells are spawners - they reside on the blood Vessels and don't move
    bool isSpawner = false;

    // for newly spawned TCells to be initialized
    bool isInitialized;
    int requestedPlace;

    // for testing
    ostringstream convert;

    void *init();

    void *requestMove();

    void *move();

    void *addMigrationData();

    void *modifyMigrationData();

    void *arrival();

    void *spawnTCell();

    void *newTCellInit();

    void *printName() {
        convert.str("");
        convert << agentId << endl;
        MASS_base::log(convert.str());
        return NULL;
    }

    void *printLocation() {
        if (!isSpawner) {
            convert.str("");
            convert << agentId << " at " << index[0] << " , " << index[1] << endl;
            MASS_base::log(convert.str());
        }
        return NULL;
    }

    void *writeTCellToXML();

    // migratable TCell data for serialization
    struct tCell_migratable_data {
        // only spawnPtNum TCells are spawners - they reside on the blood Vessels and don't move
        bool _isSpawner;

        // for newly spawned TCells to be initialized
        bool _isInitialized;
        int _requestedPlace;
    };
};
#endif //TCELL_H
