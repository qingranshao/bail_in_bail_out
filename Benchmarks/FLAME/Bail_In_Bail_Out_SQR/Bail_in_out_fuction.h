#include "header.h"
#include "firms_agent_header.h"
#include "households_agent_header.h"
#include "banks_agent_header.h"

#include <time.h>
#include <stdlib.h>// for random number.

#define m 0.8
#define o 0.2
#define x 0.1
#define II 0.8

#define READY 0
#define RUNNING 1
#define STOP 2

int TIMER_STATUS_H = READY;
int STATE_SIGNAL = 1;
double modifier = 1.0;

double start,stop,total_time;
/***************************
***.H additional functions**
***************************/

void run_timer()
{
    if(TIMER_STATUS_H == READY)
    {
        start = get_time();
    }
    else if(TIMER_STATUS_H == RUNNING)
    {
        stop = get_time();
        total_time = stop - start;
        printf("Execution time - %d:%02d:%03d [mins:secs:msecs]\n",
       (int)(total_time/60), ((int)total_time)%60, (((int)(total_time * 1000.0)) % 1000));
    }
    TIMER_STATUS_H +=1;
}

/***************************
***Firms agent functions****
***************************/

//Input he cost and return the new COST.
int production_cost(int cost)
{
    srand((unsigned)F_ID+1000);
    modifier = (float)rand()/RAND_MAX + 0.5;
    cost *= modifier;
    return cost;
}

// The PROFIT should take the return value
int calculate_profit(double cost)
{
    return (cost*modifier) - cost;
}

// require profit > 0
void calculate_dividends(double *profit)
{
    *profit *= m;
}

double calculate_liqudity(double fliq, double profit)
{
    fliq += profit;
    return fliq;
}

/***************************
*House hold agent functions*
***************************/

/**
 *calculate consume, add change the remain wages/dv
 *nothing need to ouput.
 *intput wages
 */
void consume(double *wage)
{
    *wage *= (1-m);
}



/***************************
****Banks agent functions***
***************************/
/**
 *update the Banks interest rate
 *@param Liquidity 
 */

void update_liq(double *liq)
{
    srand(BANK_ID);
    int r = rand()%40;
    *liq = (0.8 + (double)(r/100)) * II;
}

