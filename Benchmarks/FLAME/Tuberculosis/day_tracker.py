class Day_Tracker(object):

    def __init__(self):
        self.day = 0

    def __repr__(self):
        return """
        <xagent>
            <name>DayTracker</name>
            <day>{day}</day>
        </xagent>""".format(day=self.day)