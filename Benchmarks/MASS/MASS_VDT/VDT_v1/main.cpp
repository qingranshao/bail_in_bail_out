/*==============================================================================
  Virtual Development Team Simulation
  Author: Ian Dudder
================================================================================
  The Virtual Development Team (VDT) benchmark program uses MASS C++ framework
  to model software engineering teams completing different projects. This 
  simulation uses Engineer Agents working on Team Places to complete Projects.
==============================================================================*/

#include "MASS.h"
#include "Task.h"
#include "Team.h"     // Place
#include "Engineer.h" // Agent
#include "Timer.h"	  // Timer
#include <stdlib.h>   // atoi

Timer timer;
ostringstream logText;

bool readTasks(string filename, Task** collabTasks, Task** prodTasks,
  int& numCollabs, int& numProds);


int main( int argc, char *args[] ) {
	// Check for arguments
	if ( argc != 11 ) {
		cerr << "usage: ./main username password machinefile port nProc nThreads"
      << "numTurns sizeX sizeY" << endl;
		return -1;
	}
  
	//---------------------- Get the arguments passed in -------------------------
	char *arguments[4];
	arguments[0] = args[1];               // Username
	arguments[1] = args[2];               // Password
	arguments[2] = args[3];               // machinefile
	arguments[3] = args[4];               // Port Number
	int nProc = atoi( args[5] );          // number of processes
	int nThr = atoi( args[6] );           // number of threads
	const int numTurns = atoi( args[7] );	// Run this simulation for numTurns
	const int sizeX = atoi( args[8] );    // Number of columns in Place
	const int sizeY = atoi( args[9] );    // Number of rows in Place
  const string inputFile = args[10];    // Tasklist file name
  char* dummyArg = " ";
  int teamsHandle = 1;
  int engineersHandle = 2;
	

	/* Initialize MASS with the machine information, number of processes, and 
   * number of threads. */
	MASS::init( arguments, nProc, nThr ); 
  timer.start(); // Evaluation begins now

  //----------------------------- Read in Tasks---------------------------------
  int numCollabTasks = 0, numProdTasks = 0;
  Task* collabTasks = nullptr;
  Task* prodTasks = nullptr;

  /*Debug*/logText.str("");
  /*Debug*/logText << "Checkpoint 1: Before reading tasks" << endl;
  /*Debug*/MASS_base::log(logText.str());

  readTasks(inputFile, &collabTasks, &prodTasks, numCollabTasks, numProdTasks);

  /*Debug*/
    logText.str("");
    logText << "Checkpoint 2: After reading tasks" << endl;
    logText << "Tasks" << endl;
    MASS_base::log(logText.str());
  /*Debug*/

  //---------------------------- Initialize Teams ------------------------------
  /*Debug*/logText.str("");
  /*Debug*/logText << "Checkpoint 3: Creating Teams..." << endl;
  /*Debug*/MASS_base::log(logText.str());

  Places* teams = new Places(teamsHandle, "Team", 1, dummyArg, 1, 2, sizeX, sizeY);

  pair<int,int> numtasks = {numProdTasks, numCollabTasks};
  teams->callAll(Team::init_, (void*)&numtasks, sizeof(pair<int,int>));

  /*Debug*/teams->callAll(Team::testConstruction_, dummyArg, 1);
  /*Debug*/logText.str("");
  /*Debug*/logText << "Checkpoint 4: Giving Tasks to Teams" << endl;
  /*Debug*/MASS_base::log(logText.str());
  
  teams->callAll(Team::initProduction_, (void*)prodTasks, sizeof(Task) * numProdTasks);

  /*Debug*/teams->callAll(Team::testProdInit_, dummyArg, 1); 

  


  //---------------------------- Simulation Loop -------------------------------
	for (int turn = 0; turn < numTurns; turn++) {    
    // Have Engineers work on tasks
    // Have Teams update the time and check the projects for completion
	}
	
	long elaspedTime_END =  timer.lap();
  logText.str("");
  logText << endl << "End of simulation. Elapsed time using MASS framework with" 
    << nProc << " processes, " << nThr << " threads, and " << numTurns << "turns"
    << " is: " << elaspedTime_END << endl << endl;
  MASS_base::log(logText.str());

	MASS::finish( );
}


bool readTasks(string filename, Task** collabTasks, Task** prodTasks, int& numCollabs, int& numProds) {
  ifstream taskFile;
  taskFile.open(filename);

  if (!taskFile) {
    cerr << "Failed to open file" << endl;
    return false;
  }
  else {
    string curRead;
    int prodIndex = 0, collabIndex = 0;
    while(!taskFile.eof()) {
      getline(taskFile, curRead, ':');
      if (curRead == "Collaboration") {
        taskFile >> numCollabs;
        (*collabTasks) = new Task[numCollabs];
        taskFile.ignore(1,'\n');
      }
      else if (curRead == "Production") {
        taskFile >> numProds;
        (*prodTasks) = new Task[numProds];
        taskFile.ignore(1,'\n');
      }
      else if (curRead == "toMeet" || curRead == "Hours") {
        vector<int> args;   // Stores Task arguments
        // Get Task Hours
        for (int englvl = 0; englvl < 5; englvl++) {
          getline(taskFile, curRead, ',');
          args.push_back(stoi(curRead));
        }
        // Read other values
        bool endLine = false;
        while(!endLine) {
          getline(taskFile, curRead, ':'); //Throw away identifier
          if (curRead == " project") {
            getline(taskFile, curRead, '\n'); //Throw away endline
            endLine = true;
          }
          else {
            getline(taskFile, curRead, ','); //Throw away comma
          }
          args.push_back(stoi(curRead));  // Convert to integer
        }
        // Assign Values to task
        if (args[5] == 1 && collabIndex < numCollabs) {
          for (int engHr = 0; engHr < 5; engHr++) {
            (*collabTasks)[collabIndex].setHours(engHr, args[engHr]);
          }
          (*collabTasks)[collabIndex].setType(args[5]);
          (*collabTasks)[collabIndex].setPriority(args[6]);
          (*collabTasks)[collabIndex].setTotalHours(args[7]);
          (*collabTasks)[collabIndex].setTaskId(args[8]);
          (*collabTasks)[collabIndex].setCollabDay(args[9]);
          (*collabTasks)[collabIndex].setProjectId(args[10]);
          collabIndex++;
        }
        else if (args[5] == 0 && prodIndex < numProds) {
          for (int engHr = 0; engHr < 5; engHr++) {
            (*prodTasks)[prodIndex].setHours(engHr, args[engHr]);
          }
          (*prodTasks)[prodIndex].setType(args[5]);
          (*prodTasks)[prodIndex].setPriority(args[6]);
          (*prodTasks)[prodIndex].setTotalHours(args[7]);
          (*prodTasks)[prodIndex].setTaskId(args[8]);
          (*prodTasks)[prodIndex].setProjectId(args[9]);
          prodIndex++;
        }
        //Throw away leftover endline
        if (taskFile.peek() == '\n') {
          taskFile.ignore(1,'\n');
        }
      }
    }
  }
  taskFile.close();
  return true;
}