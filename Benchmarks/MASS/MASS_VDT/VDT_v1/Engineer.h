/*==============================================================================
  Engineer Class
  Author: Ian Dudder
================================================================================
  The Engineer Class will take the role of Agents in this simulation. There are
  five different types of Engineer Agents:
    1. Project Leads
    2. UX Designers
    3. Senior Software Developers
    4. Junior Software Developers
    5. Test Engineers
  These Engineers will process tasks in a hierarchy structured in the order the
  agents are listed above. Engineer Agents will select a Task to work on and
  work on it until it is complete or they have a collaboration task to tend to.
==============================================================================*/

#ifndef ENGINEER_H
#define ENGINEER_H

#include <iostream>
#include "Agent.h"
#include "MASS_base.h"
#include "Task.h"
#include "Team.h"


class Engineer : public Agent {
public:
  //=============================== Function Ids ===============================
  static const int init_ = 0;
  static const int work_ = 1;

  //============================== Public Methods ==============================
  Engineer(void* argument) : Agent(argument) {}

  //------------------------------- Call Method --------------------------------
  virtual void* callMethod(int functionId, void* argument) {

    switch(functionId) {
      case init_ :
        return init();

      case work_ :
        return work(argument);

      default :
        return badFunctionId(functionId);
    }
    return nullptr;
  }

private:
  //============================= Private Methods ==============================

  //---------------------------------- init ------------------------------------
  void* init();


  //---------------------------------- work ------------------------------------
  /** Has the Engineer Agent work for an hour on a Task. 
   *  @param dayAndHour - a pair<int,int> object with the first item as the day
   *         and the second item as the hour.                                  
   */
  void* work(void* dayAndHour);


  //------------------------------ checkForCollab ------------------------------
  bool checkForCollab();


  //--------------------------------- getTask ----------------------------------
  /** Gets a new task for the Engineer to work on.
   *  @return - returns true if a task was selected and false if the task was
   *            given up due to an exception.                                 
   */
  bool getTask();


  //---------------------------- handleTaskException ---------------------------
  /** Deals with an exception occuring while selecting a new task.
  */
  bool handleTaskException();


  //-------------------------------- submitTask --------------------------------
  /** Returns the task either to the next tray down in the hierarchy or to the
   *  completed task list.
   */
  bool submitTask();


  //----------------------------- badFunctionId --------------------------------
  /** Prints a MASS_log if an invalid functionId was called.                  
  */
  void* badFunctionId(int functionId) const;


  //================================ Member Data ===============================
  int type;               // Type of Engineer Agent (e.g., Lead, Senior, etc.)
  
  Task* currentTask;      // Pointer to the current Task in progress
  ostringstream logText;  // Used to create MASS logs
};

#endif //ENGINEER_H

//----------------------------------------------------------------------------
//============================================================================