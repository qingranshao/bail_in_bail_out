

#include "Project.h"
#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/Point.h"

RepastHPCAgent::RepastHPCAgent(repast::AgentId id): id_(id), infected(200){ }

RepastHPCAgent::RepastHPCAgent(repast::AgentId id, bool newInfected): id_(id), infected(newInfected){ }

RepastHPCAgent::~RepastHPCAgent(){ }


void RepastHPCAgent::set(int currentRank, bool newInfected){
    id_.currentRank(currentRank);
    infected = rand() %2;
    agentType = rand() % 3;
    agentEnergy = 8;

    counter = 0;
   if (agentType == 1) {
       numWorkers = rand() % 10 + 1;
       totResource = rand() % 150 + 900;
       totIntell = rand() % 15 + 100;
       durWork = rand() % 20 + 10;
       jobIndex = rand() % 10;
    }
    else {
       numWorkers = -1;
       totResource = -1;
       totIntell = -1;
       durWork = -1;
       jobIndex = -1;
    }


}




void RepastHPCAgent::play(repast::SharedContext<RepastHPCAgent>* context,
                              repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space){
    std::vector<RepastHPCAgent*> agentsToPlay;
    

    if (agentType == 1 && jobIndex == counter) {
       std::vector<int> agentLoc;
       space->getLocation(id_, agentLoc);
       repast::Point<int> center(agentLoc);
       repast::Moore2DGridQuery<RepastHPCAgent> moore2DQuery(space);
       moore2DQuery.query(center, 1, false, agentsToPlay);





       int currWorkers = 0;
       int leaderCount = 0;


       std::vector<RepastHPCAgent*>::iterator agentToPlay = agentsToPlay.begin();
       while (agentToPlay != agentsToPlay.end()) {
          std::vector<int> otherLoc;
          space->getLocation((*agentToPlay)->getId(), otherLoc);
          repast::Point<int> otherPoint(otherLoc);


          if ((*agentToPlay)->getAgentType() == 1) {
             leaderCount++;
          }
          else {						// increment firmCount
             currWorkers++;
          }

          agentToPlay++;
       }

       int missingWorkers = 0;
       int missingIntell = 0;
       int missingResources = 0;

       int currResources = currWorkers * 100;
       int currIntell = currWorkers * 10;
       if (numWorkers > currWorkers) {
          missingWorkers = numWorkers - currWorkers;
       }
       if (totResource > currResources) {
          missingResources = totResource - currResources;
          int tempResources = missingResources / 100;
          if (missingResources % 100 > 0) {
             missingResources = tempResources + 1;
          }
          else {
             missingResources = tempResources;
          }
       }
       if (totIntell > currIntell) {
          missingIntell = totIntell - currIntell;
          int tempIntell = missingIntell / 10;
          if (missingIntell % 10 > 0) {
             missingIntell = tempIntell + 1;
          }
          else {
             missingIntell = tempIntell;
          }
       }


       int max = (missingWorkers < missingResources) ? missingResources : missingWorkers;
       int numToSpawn = ((max < missingIntell) ? missingIntell : max);
       int spawnCounter = 0;

       agentToPlay = agentsToPlay.begin();
       while (agentToPlay != agentsToPlay.end()) {
          std::vector<int> otherLoc;
          space->getLocation((*agentToPlay)->getId(), otherLoc);
          repast::Point<int> otherPoint(otherLoc);


          if ((*agentToPlay)->getAgentType() == 2 && spawnCounter < numToSpawn) {
            (*agentToPlay)->setMoveLocX(otherLoc.at(0));
            (*agentToPlay)->setMoveLocY(otherLoc.at(1));
            (*agentToPlay)->setDurWork(durWork);
            (*agentToPlay)->setJobIndex(jobIndex);
          }

          agentToPlay++;
       }
    }
}

void RepastHPCAgent::playAgain(repast::SharedContext<RepastHPCAgent>* context,
   repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space) {
   std::vector<RepastHPCAgent*> agentsToPlay;

   if (agentType != 1 && jobIndex == counter) {
      for (int i = 0; i < 30; i++) {
         std::cout << "worker: " << id_ << "has worked " << i << "work cycle." << std::endl;
      }
   }
   counter++;
}


void RepastHPCAgent::move(repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space) {
   
   if (agentType == 2) {
      std::vector<int> agentNewLoc;
      agentNewLoc.push_back(locX);
      agentNewLoc.push_back(locY);
      space->moveTo(id_, agentNewLoc);

   }
}

/* Serializable Agent Package Data */

RepastHPCAgentPackage::RepastHPCAgentPackage(){ }

RepastHPCAgentPackage::RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank, bool _infected, int _agentType, int _agentEnergy, int _numWorkers, int _totResource, int _totIntell, int _durWork, int _status):
id(_id), rank(_rank), type(_type), currentRank(_currentRank), infected(_infected), agentType(_agentType), agentEnergy(_agentEnergy), numWorkers(_numWorkers), totResource(_totResource), totIntell(_totIntell), durWork(_durWork), status(_status){ }
