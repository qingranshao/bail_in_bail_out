

#ifndef BAILIN
#define BAILIN

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"


/* Agents */
class RepastHPCAgent{
	
private:
    repast::AgentId   id_;
    int           financeType; //0 = firm, 1 = bank
    int           amountOfMoney;
	
    vector<int> firmsServiced;
public:
    RepastHPCAgent(repast::AgentId id);
	RepastHPCAgent(){}

    ~RepastHPCAgent();
	
    /* Required Getters */
    virtual repast::AgentId& getId(){                   return id_;    }
    virtual const repast::AgentId& getId() const {      return id_;    }
	
    /* Getters specific to this kind of Agent */
    int  getFinanceType() { return financeType; }
    int  getAmountOfMoney() { return amountOfMoney; }
    void  setAmountOfMoney(int newAmtOfMoney) { amountOfMoney = newAmtOfMoney; }
	
    /* Setter */
    void set(int currentRank);
	
    /* Actions */
    void play(repast::SharedContext<RepastHPCAgent>* context,
              repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space);    // Choose three other agents from the given context and see if they cooperate or not
    void playAgain(repast::SharedContext<RepastHPCAgent>* context,
       repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space);
    void move(repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space);
};

/* Serializable Agent Package */
struct RepastHPCAgentPackage {
	
public:
    int    id;
    int    rank;
    int    type;
    int    currentRank;
    int financeType;
    int amountOfMoney;

    vector<int> firmsServiced;
	
    /* Constructors */
    RepastHPCAgentPackage(); // For serialization
    RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank, int _financeType, int _amountOfMoney);
	
    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        ar & financeType;
        ar & amountOfMoney;
    }
	
};


#endif