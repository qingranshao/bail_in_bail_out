# Docker support for TB-Simulation
---
## General Requirements
docker version >= 17.09

docker-compose version >= 3.4

---
## Deploying on 1 node

### Preparations
1. Create your own public/private keys pairs in directory dependencies/, name the public key as **key.pub** and private key as **key**

### Command to build/run docker containers
Execute all commands below in the root directory of the application.
#### To build docker images
    docker-compose build
#### To launch containers
    docker-compose --compatibility up -d

#### To execute the TB-Simulation application
    ./debugRun

---
## Q&A
### How to re-run application?
**Re-run application without any code changes**

Just execute "./debugRun" again in the master container

**Re-run application with code changes**

Start from **To build docker images** and follows the instructions till **To execute the TB-Simulation application**
