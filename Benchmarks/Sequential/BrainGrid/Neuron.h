#ifndef NEURON_H
#define NEURON_H
#include <stdlib.h> // cout/cin/cerr/endl
#include <cstddef> // NULL, since nullptr defined in c++11
#include <algorithm> // std::fill

 // Find Grow Source, Find Connect Source
 // Use Connect Source to Search Prev Connections
 // If no redundant connection SetConnect (Both Sources)
 // If redundant connection setStop (Grow Source)
class Neuron {
  public:
    // Enums to indicate the base type of Neuron
    enum Type {ACTIVE=0, INACTIVE, NEUTRAL};
    // Enums to determine reaction of previous simulation iteraion(s)
    enum Stage {VISIT1=0, VISIT2, ENACTED, CONNECTED, STOPPED, INDEF};

    // Constructor & Destructor
    Neuron();
    ~Neuron();

    // Diffuses a "CONNECTED" flag to the entire synapse
    void SetConnect();
    // Diffuses a "STOPPED" flag to the entire synapse
    void SetStop();

    Type neuronType;
    Stage neuronStage;

    // Value determined by initial direction and additive complements i.e. (8 - x).
    int nextCell;
    // Representative of a Neuron's direct knowledge of the synapse
    // For Active (8 neighbors), else (3 neighbors)
    Neuron** neighbors;
};

#endif
