#ifndef GRAPH_H
#define GRAPH_H
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h> /* exit, EXIT_FAILURE */

class Graph {
  public:
    Graph(std::string filename);

    // Nodes that comprise Graph
    struct Process {
      // Metadata
      int processId;
      int currentTeam = -1;
      bool isCompleted = false;

      // Graph connections
      int completedParents = 0;
      std::vector<Process*> prev;
      std::vector<Process*> next;

       // Process Constraints
      int numWorkerReq = 0;
      int resourcesReq = 0;
      int knowledgeReq = 0;
      int minTime = 0;

      // Process Basic Constructor
      Process(int id) {
        processId = id;
      }

      // Process Constructor
      Process(int id, int workers, int rsc, int intel, int timespan) {
        processId = id;
        numWorkerReq = workers;
        resourcesReq = rsc;
        knowledgeReq = intel;
        minTime = timespan;
      }

    } *start, *end;

//  private:
    // A numbered off collection of processes
    Process** processes;
    int processesSize;
};

#endif