#include "Neuron.h"

// Constructor and Destructor
Neuron::Neuron() {
  // Bias-Randomize neuronType
  double r = ((double) rand() / (RAND_MAX));
  if (r <= 0.1) neuronType = Neuron::Type::ACTIVE;
  else if (r <= 0.3) neuronType = Neuron::Type::INACTIVE;
  else neuronType = Neuron::Type::NEUTRAL;

  // Initialized at -1, since value perpetuates growth direction
  nextCell = -1;

  // Set Neuron Stage
  if(neuronType != ACTIVE) neuronStage = INDEF;
  else neuronStage = VISIT1;

  // Set Neuron Neighbors
  if (neuronType == ACTIVE) {
    // Moore's Neighborhood (i.e. 8 neighborhoods)
    neighbors = new Neuron*[8];
    std::fill(neighbors, neighbors + 8, nullptr);
  }
  else if (neuronType == NEUTRAL) {
    // Accomodate Synapse Interruption: 0 - Src, 1 - Dest, 2 - Intercede
    neighbors = new Neuron*[3];
    std::fill(neighbors, neighbors + 3, nullptr);
  }
  else neighbors = nullptr;
//  std::cout << stage << std::endl;
}

// Remove pointer to other Neurons
Neuron::~Neuron() {
  // Setting up the deleting forloop
  int size = 0;
  if (neuronType == ACTIVE) size = 8;
  else if (neuronType == NEUTRAL) size = 3;

  // "Empty" but do not destory neighbors
  for (int i = 0; i < size; i++) {
    neighbors[i] = nullptr;
    delete neighbors[i];
  }
}

// Helper Function for DEST to CONNECT the 2 or 3 neighbors
void Neuron::SetConnect() {
   Neuron* tmp = nullptr;

   for (int i = 0; i < 3; i++)    {
      if (this->neighbors[i] != nullptr) {
         tmp = this->neighbors[i];
         while (tmp != nullptr) {
            if (tmp->neuronType == ACTIVE) break;

            tmp->neuronStage = CONNECTED;
            tmp = tmp->neighbors[0];
         }
      }
   }
}

// Helper Function for SRC Neuron to STOP Growth
void Neuron::SetStop() {
  if (neuronType != ACTIVE) {
     Neuron* tmp = this;
     while (tmp != nullptr) {
        if (tmp->neuronType == ACTIVE) break;

        tmp->neuronStage = STOPPED;
        tmp = tmp->neighbors[0];
     }
  }
}
