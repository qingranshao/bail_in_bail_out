class Agent_Factory(object):

    def __repr__(self):
        return """
        <xagent>
            <name>AgentFactory</name>
            <available_macrophage_id>{available_macrophage_id}</available_macrophage_id>
            <available_t_cell_id>{available_t_cell_id}</available_t_cell_id>
        </xagent>""".format(available_macrophage_id=100, available_t_cell_id=0)