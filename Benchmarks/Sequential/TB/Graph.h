#ifndef GRAPH_H
#define GRAPH_H
#include <vector>
#include <cstddef>
#include <iostream>
#include <stdlib.h> 
#include <time.h>

class Graph {
  public:
    // Mode Enumerations for Environmental Changes
    enum Mode {BACTERIA = 0, CLEAR, ALERT, AGENTS};

    // Constructor
    Graph(int size) {
      graphSize = size;
      // Construct environment
      T = new Place**[size];
      for(int i=0; i<size; i++) {
        T[i] = new Place*[size];
        for(int j=0; j<size; j++) {
          T[i][j] = new Place[2];
        }
      }
    }
    // Deconstructor
    // Note utilizes the deconstructor for Place object
    ~Graph() {
      int size = graphSize;
      for(int i=0; i<size; i++)
      {
        for(int j=0; j<size; j++) {
          delete T[i][j];
          T[i][j] = nullptr;
        }
        delete T[i];
        T[i] = nullptr;
      }
      delete T;
      T = nullptr;
    }

    void simulate(int iterations);
    void display(Graph::Mode mode); // Assumed display of current space
  private:
    // Comprises Agents
    struct Agent {
        // General Specs
        int agentId = -1;
        int location[2][2]; // Current and Next Space
        int agentLifeSpan = 2;
//        int visionRadius = 1;
        bool activeFlag = false;
        bool deadFlag = false;

        // Infected Specs
        bool infectedFlag = false;
        int infected_capacity = 20;
        int infected_size = 0;
        int agentSpeed = 2; // Update to 1 if infected

        // Agent Constructors
        Agent(int id, int initRow, int initCol, int turn, bool active) {
          agentId = id;
          location[turn][0] = initRow;
          location[turn][1] = initCol;
          activeFlag = active;
        }

        // Reset Agent for Later Useage
        void resetAgent() {
          location[0][0] = -1;
          location[0][1] = -1;
          location[1][0] = -1;
          location[1][0] = -1;
          agentLifeSpan = 50;
          activeFlag = false;
          infectedFlag = false;
          infected_size = 0;
          agentSpeed = 2;
        };
    };

    // Comprises Places
    struct Place{
      // Allowance for Selective Collision
      int agentIds[2];
      int agentSize;

      // Track chemical/bacteria growth
      bool bacteriaFlag = false;
      int alertFlag = 0;

      // Determines Special Entry Point Nodes
      bool entryPointFlag = false;

      // Place Constructor
      Place() {
        defaultPlace();
      }
      
      void defaultPlace() {
      	agentIds[0] = -1;
      	agentIds[1] = -1;
        agentSize = 0;
        bacteriaFlag = false;
        alertFlag = 0;
        entryPointFlag = false;
      }
    };

    // Total Infected Places
    int totalBacteria = 0;
    int bacteriaSpeed = 2;
    int bacterialDeathSpeed = 4;

    // Time till dissipation
    int alertTimeSpan = 2;
    int alertSpeed = 2;

    // Clear radius
    int clearSpeed = 2;

    // Holds Torodial Environment
    Place*** T;

    // Size of Graph
    int graphSize;

    // Global Agents Container
    std::vector<Agent> globalAgents;
    std::vector<int> freeIds;   
 
    // Simulation Update Functions
    void updatePlace(int row, int col);
    void updateAgent(int row, int col);
    void updateSingle(int row, int col, Graph::Mode mode);
    void updateNeighbors(int row, int col, int speed, Graph::Mode mode);

    // Update Helpers Functions/Variables
    int curTurn;
    int nextTurn;

    // Helper Functions
    int Torus(int val) const {
      if(val < 0) 
        { return val + graphSize; }
      else if(val >= graphSize)
        { return val - graphSize; }
      else
        { return val; }
    }
    void Disperse(int row, int col, int speed, Graph::Mode mode);
    void Decay(int row, int col, Graph::Mode mode);
    void ClearBacteria(int row, int col);
    void initEnvironment(int EPperRow);
    void makeAgent(int row, int col, bool active);
    void killAgent(int id);
    void moveAgent(int id);
};
#endif
