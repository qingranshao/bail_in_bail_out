import java.util.Arrays;
import java.util.Random;

enum neuronType {EXCITATORY, INHIBITORY, NEUTRAL}

public class WriteNeuron {
    private int numIterations;
    private int nID;
    private neuronType type;
    private int somaPlaceID;
    private Random random;

    WriteNeuron(int iterations, int id, neuronType type, int soma) {
        numIterations = iterations;
        nID = id;
        this.type = type;
        somaPlaceID = soma;
        random = new Random();
    }

    @Override
    public String toString() {
        String writeNeuron = "\t<xagent>\n";
        writeNeuron += "\t\t<name>Neuron</name>\n";
        writeNeuron += "\t\t<n_id>" + nID + "</n_id>\n";
        writeNeuron += "\t\t<neuron_type>"+ type.ordinal() + "</neuron_type" +
                       ">\n";
        writeNeuron += "\t\t<sum_of_recvd_signals>" +
                       initStateSONN.doesNotExist +
                       "</sum_of_recvd_signals>\n";
        writeNeuron += "\t\t<in_connections>{}</in_connections>\n";
        writeNeuron += "\t\t<out_connections>{}</out_connections>\n";
        writeNeuron += "\t\t<soma_place>" + somaPlaceID + "</soma_place>\n";
        writeNeuron += "\t\t<avail_dir_around_soma>{1, 1, 1, 1, 1, 1, 1, 1 }" +
                       "</avail_dir_around_soma>\n";
        writeNeuron += "\t\t<new_synap_neighbors>{}</new_synap_neighbors>\n";
        writeNeuron += "\t\t<new_dendrite_neighbors>{}" +
                       "</new_dendrite_neighbors>\n";
        writeNeuron += writeAxon();
        writeNeuron += writeDendrites();
        // grow request array
        writeNeuron += "\t\t<dendr_grow_requests>{}</dendr_grow_requests>\n";
        writeNeuron += "\t</xagent>\n";
        return writeNeuron;
    }

    private String writeAxon() {
        String writeAxon = "\t\t<this_axon>";

        int rand = random.nextInt((numIterations - 1) + 1) + 1;
        // random axon creation time
        writeAxon += "{ {" + rand +"}";
        // axon state -> 0 = NOT_CREATED
        writeAxon += ", {0}";
        // growth_after_last_branch
        writeAxon += ", {0}";
        // axon_grow_requests
        writeAxon += ", {}";
        // axon occupied places
        writeAxon += ", {" + somaPlaceID + "}";
        // synapse branches
        writeAxon += ", {0}";
        // growing ends
        writeAxon += ", {}";
        // synapse occupied places
        writeAxon += " , {}}";
        writeAxon += "</this_axon>\n";

        return writeAxon;
    }

    private String writeDendrites() {
        String writeDendrites = "\t\t<dendrites>{";
        for (int i = 0; i < initStateSONN.numDendrites; i++) {
            // random dendrite creation time
            int rand = random.nextInt((numIterations - 1) + 1) + 1;

            writeDendrites += "{ {" + rand +"}";
            // dendrite state
            writeDendrites += ", {0}";
            // branches
            writeDendrites += ", {0}";
            // growth after the last branch
            writeDendrites += ", {0}";
            // growing ends array
            writeDendrites += ", { }";
            // dendrite occupied places
            writeDendrites += ", {}}";
            if (i < initStateSONN.numDendrites - 1) {
                writeDendrites += ", ";
            }
        }
        writeDendrites += " }</dendrites>\n";
        return writeDendrites;
    }
}
