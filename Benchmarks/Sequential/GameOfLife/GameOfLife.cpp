// File: GameOfLife.cpp
// Author: Brian Tang
// Description: Game of Life Simulation (CPU Implementation)

#include <iostream>     // cout
#include <stdlib.h>     // rand
#include "Timer.h"      // timer
using namespace std;

// Used to store the current Life health status and the health status
// of the next generation of each cell in the Game of Life grid
struct Life {
	bool mod0, mod1;
};

// Computes a cell's next generation's life state
void computeDeadOrAlive(Life** grid, int size, int tick, int i, int j) {
	int numAlive = 0;
	for (int vert = -1; vert <= 1; vert++) {
		for (int hori = -1; hori <= 1; hori++) {
			if (vert != 0 || hori != 0) {
				if (i + vert != -1 && i + vert != size && j + hori != -1 && j + hori != size) {
					numAlive += tick % 2 == 0 ? grid[i + vert][j + hori].mod0 : grid[i + vert][j + hori].mod1;
				}
			}
		}
	}
	if (tick % 2 == 0) {
		grid[i][j].mod1 = grid[i][j].mod0 ? numAlive == 2 || numAlive == 3 : numAlive == 3;
	} else {
		grid[i][j].mod0 = grid[i][j].mod1 ? numAlive == 2 || numAlive == 3 : numAlive == 3;
	}
}

// Runs the Game Of Life Simulation
void runGameOfLife(int generations, int size) {
	int i, j, tick, numAlive;
	Life **grid = new Life*[size];
	for (i = 0; i < size; i++) {
		grid[i] = new Life[size];
	}

	// Initialize Game of Life Grid
	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			grid[i][j].mod0 = rand() % 2 == 0;
		}
	}

	// Simulation
	for (tick = 0; tick < generations; tick++) {
		for (i = 0; i < size; i++) {
			for (j = 0; j < size; j++) {
				computeDeadOrAlive(grid, size, tick, i, j);
			}
		}
	}

	// Display Health Status
	cout << "Health Status at Size " << size << " after " << generations << " Generations" << endl;
	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			cout << grid[i][j].mod0 << "  ";
		}
		cout << endl;
	}

	// Clear Heap Memory
	for (i = 0; i < size; i++) {
		delete[] grid[i];
	}
	delete[] grid;
}

// Runs the Game Of Life simulation on multiple simulation sizes
int main() {
	const int numSimSizes = 5, generations = 10;
	const int simSizes[numSimSizes] = {2, 4, 8, 16, 32};

	Timer timer;
	cout << "Game of Life (CPU Implementation)" << endl;

	cout << "Starting Simulation" << endl;
	for (int i = 0; i < numSimSizes; i++) {
		timer.start();
		runGameOfLife(generations, simSizes[i]);
		cout << "Elapsed Time: " << timer.lap() << "ms" << endl << endl;
	}
	cout << "Ending Simulation";

	return 0;
}
