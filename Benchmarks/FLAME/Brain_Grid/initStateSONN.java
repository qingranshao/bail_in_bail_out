import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

/**
 * author : Sarah Panther
 * since  : November 18th, 2019
 * <p>
 * CSS 499 - MASS C++ Benchmarking and Comparison
 * <p>
 * Self-Organizing Neural Network simulation - 0.xml initialization program
 * <p>
 * Please see the Self-Organizing Neural Network Specification for details.
 */

public class initStateSONN {
    private static int iterations;
    private static int sSize;

    private static int numNeurons;

    private static int numExcitatory;
    private static int numInhibitory;
    private static int numNeutral;

    // simulation constants
    private static int activatingSignal;
    private static int signalModulation;
    private static int growingMode;
    private static int repetitiveBranches;
    private static int branchPossibility;
    private static int branchGrowth;
    private static int stopBranchGrowth;
    private static double thresholdSignal;

    // default values
    static final int doesNotExist = -1;
    static final int numDendrites = 7;

    static final int defaultSize = 100;
    static final int smallPercentage = 1;
    static final int largePercentage = 90;
    static final int defaultRepetitiveBranch = 3;
    static final int defaultBranchChance = 33;
    static final int defaultBranchGrowth = 5;
    static final double defaultThreshold = .4;

    private static ArrayList<Integer> places;
    private static ArrayList<WriteNeuron> neurons;

    public static void main(String[] args) {
        checkInput(args);
        createPlaces();
        createNeurons();
        writePlaceandNeuronXML();
    }

    private static void checkInput(String[] args) {
        if (!(args.length == 2 && args[1].toUpperCase().equals("Y")) &&
                !(args.length == 13 && args[1].toUpperCase().equals("N"))) {
            System.out.println(
                    "Usage: java init_state_SONN (# of iterations) y/n(use " +
                            "default values): if no ->(grid side length) " +
                            "(# of neurons) (# of excitatory cells) " +
                            "(# of inhibitory cells) " +
                            "(A - activation signal % chance of occurrence) " +
                            "(M - excitatory amplification %) " +
                            "(G - growing mode % probability) " +
                            "(R - max # of branches) " +
                            "(B - branching % chance for this time step) " +
                            "(K - # of times part grows after last branch) " +
                            "(T - threshold signal value as a double))");

            System.exit(1);
        }

        iterations = Integer.parseInt(args[0]);

        String defaultVals = args[1];

        if (defaultVals.toUpperCase().equals("N")) {

            sSize = Integer.parseInt(args[2]);

            numNeurons = Integer.parseInt(args[3]);

            if (numNeutral > (sSize * sSize) / 2) {
                System.out.println("ERROR: Too neurons for simulation space  " +
                        "size. Number of neurons less than 50% of total " +
                        "number of spaces\nExiting now");
                System.exit(-1);
            }
            numExcitatory = Integer.parseInt(args[4]);
            numInhibitory = Integer.parseInt(args[5]);
            numNeutral = numNeurons - numExcitatory - numInhibitory;

            if (numNeutral <= 0) {
                System.out.println("ERROR: Too many exitatory and/or " +
                        "inhibitory neurons for total neuron number.  Number" +
                        " of neutral neurons must at least be 1\nExiting " +
                        "now");
                System.exit(-1);
            }
            activatingSignal = Integer.parseInt(args[6]);
            signalModulation = Integer.parseInt(args[7]);
            growingMode = Integer.parseInt(args[8]);
            repetitiveBranches = Integer.parseInt(args[9]);
            branchPossibility = Integer.parseInt(args[10]);
            branchGrowth = Integer.parseInt(args[11]);
            stopBranchGrowth = 100 - growingMode;
            thresholdSignal = Double.parseDouble(args[12]);
        } else {
            sSize = defaultSize;
            double percent = smallPercentage * .01;

            numExcitatory = (int) (Math.pow(sSize, 2) * percent);
            System.out.println("excitatory = " + numExcitatory);
            numInhibitory = (int) (Math.pow(sSize, 2) * percent);
            System.out.println("inhibitory = " + numInhibitory);
            numNeutral = (int) (Math.pow(sSize, 2) * percent);
            System.out.println("neutral = " + numNeutral);
            numNeurons = numExcitatory + numInhibitory + numNeutral;

            activatingSignal = smallPercentage;
            signalModulation = smallPercentage;
            growingMode = largePercentage;
            repetitiveBranches = defaultRepetitiveBranch;
            branchPossibility = defaultBranchChance;
            branchGrowth = defaultBranchGrowth;
            stopBranchGrowth = 100 - growingMode;
            thresholdSignal = defaultThreshold;
        }
    }

    static void createPlaces() {
        places = new ArrayList<>();
        for (int i = 0; i < (sSize * sSize); i++) {
            places.add(i);
        }
//        System.out.println("Number of Places = " + places.size());
    }

    static void createNeurons() {
        neurons = new ArrayList<>();

        ArrayList<Integer> shuffledPlaces = new ArrayList<>();
        shuffledPlaces = places;
        Collections.shuffle(shuffledPlaces);

        // add Excitatory neurons
        for (int i = 0; i < numExcitatory; i++) {
            //System.out.println("place index : " + placeIndex);
            neurons.add(new WriteNeuron(iterations, i,
                        neuronType.EXCITATORY, shuffledPlaces.get(i)));
        }

        // add Inhibitory neurons
        for (int i = numExcitatory; i < numInhibitory + numExcitatory; i++) {
            //System.out.println("place index : " + placeIndex);
            neurons.add(new WriteNeuron(iterations, i,
                    neuronType.INHIBITORY, shuffledPlaces.get(i)));
        }


        // add Neutral neurons
        for (int i = numExcitatory + numInhibitory; i < numNeurons; i++) {
            neurons.add(new WriteNeuron(iterations, i,
                    neuronType.NEUTRAL, shuffledPlaces.get(i)));
        }
    }

    /**
     * Writes the starting parameters of the simulation based on the user
     * input to a file in the same directory named "0.xml"
     */
    // Fix
    private static void writePlaceandNeuronXML() {
        File file = new File("./0.xml");
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(file));
            pw.write("<states>\n");
            pw.write("<itno>0</itno>\n");

            // write environmental constants
            pw.write("<environment>\n");
            pw.write("<S>" + sSize + "</S>\n");
            pw.write("<A>" + activatingSignal + "</A>\n");
            pw.write("<M>" + signalModulation + "</M>\n");
            pw.write("<G>" + growingMode + "</G>\n");
            pw.write("<R>" + repetitiveBranches + "</R>\n");
            pw.write("<B>" + branchPossibility + "</B>\n");
            pw.write("<K>" + branchGrowth + "</K>\n");
            pw.write("<STOP>" + stopBranchGrowth + "</STOP>\n");
            pw.write("<T>" + thresholdSignal + "</T>\n");
            pw.write("</environment>");

            // write Place agents
            for (int i = 0; i < places.size(); i++) {
                pw.write("\t<xagent>\n");
                pw.write("\t\t<name>Place</name>\n");
                pw.write("\t\t<p_id>" + i + "</p_id>\n");
                pw.write("\t\t<occupying_n_id>" +
                        doesNotExist + "</occupying_n_id>\n");
                pw.write("\t\t<part_type>" + doesNotExist + "</part_type>\n");
                pw.write("\t</xagent>\n");
            }

            // write Neurons
            for (WriteNeuron neuron : neurons) {
                pw.write(neuron.toString());
            }
            // write Iteration Tracker
            pw.write("\t<xagent>\n");
            pw.write("\t\t<name>IterationTracker</name>\n");
            pw.write("\t\t<iteration>1</iteration>\n");
            pw.write("\t</xagent>\n");

            pw.write("</states>\n");
            pw.flush();
            pw.close();
        } catch (IOException exc) {
            System.out.println("IO Exception: " + exc.getMessage());
        }
    }
}
