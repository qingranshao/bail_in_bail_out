/*==============================================================================
  Virtual Development Team Simulation
  Author: Ian Dudder
================================================================================
  The Virtual Development Team (VDT) benchmark program uses MASS C++ framework
  to model software engineering teams completing different projects. This 
  simulation uses Engineer Agents working on Team Places to complete Projects.
==============================================================================*/

#include "MASS.h"
#include "Task.h"
#include "Team.h"     // Place
#include "Project.h"
#include "Engineer.h" // Agent
#include "Timer.h"	  // Timer
#include <stdlib.h>   // atoi

Timer timer;
ostringstream logText;

bool readTasks(string filename, Task** collabTasks, Task** prodTasks,
  int& numCollabs, int& numProds);

int main( int argc, char *args[] ) {
	// Check for arguments
	if ( argc != 11 ) {
		cerr << "usage: ./main username password machinefile port nProc nThreads"
      << "numTurns sizeX sizeY" << endl;
		return -1;
	}
  
	// Get the arguments passed in -----------------------------------------------
	char *arguments[4];
	arguments[0] = args[1];               // Username
	arguments[1] = args[2];               // Password
	arguments[2] = args[3];               // machinefile
	arguments[3] = args[4];               // Port Number
	int nProc = atoi( args[5] );          // number of processes
	int nThr = atoi( args[6] );           // number of threads
	const int numTurns = atoi( args[7] );	// Run this simulation for numTurns
	const int sizeX = atoi( args[8] );    // Number of columns in Place
	const int sizeY = atoi( args[9] );    // Number of rows in Place
  const string inputFile = args[10];    // Tasklist file name
	

	/* Initialize MASS with the machine information, number of processes, and 
   * number of threads. */
	MASS::init( arguments, nProc, nThr ); 
  timer.start(); // Evaluation begins now

  //----------------------------- Read in Tasks---------------------------------
  int numCollabTasks = 0, numProdTasks = 0;
  

  //---------------------------- Simulation Loop -------------------------------
	for (int turn = 0; turn < numTurns; turn++) {    
    // Have Engineers work on tasks
    // Have Teams update the time and check the projects for completion
	}
	
	long elaspedTime_END =  timer.lap();
  logText.str("");
  logText << endl << "End of simulation. Elapsed time using MASS framework with" 
    << nProc << " processes, " << nThr << " threads, and " << numTurns << "turns"
    << " is: " << elaspedTime_END << endl << endl;
  MASS_base::log(logText.str());

	MASS::finish( );
}


bool readTasks(string filename, Task** collabTasks, Task** prodTasks,
    int& numCollabs, int& numProds) {

  bool successfulRead = false;
  ifstream taskFile;
  taskFile.open(filename);

  if (!taskFile) {
    successfulRead = false;
  }
  else {
    
  }
  taskFile.close();
  return successfulRead;
}