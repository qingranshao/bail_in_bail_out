/* Craig Shih
* Street.cpp
* which utilizes the MASS library
*/

#include "Street.h"
#include "MASS_base.h"
#include <stdlib.h>  //rand
#include <sstream>     // ostringstream
#include <fstream>
#include <set>
#include <algorithm>
#include <stack>
#include <limits.h>
#include <cstdint>
#include <iterator>


extern "C" Place* instantiate( void *argument ) {
  return new Street( argument );
}

extern "C" void destroy( Place *object ) {
  delete object;
}

/**
* Initializes a Cluster object.
*/
void *Street::init(void *argument) {

   status = 0;

   //Define cardinals
   int north[2] = { 0, 1 };  cardinals.push_back(north);
   int east[2] = { 1, 0 };  cardinals.push_back(east);
   int south[2] = { 0, -1 }; cardinals.push_back(south);
   int west[2] = { -1, 0 }; cardinals.push_back(west);
   int northwest[2] = { -1, 1 };  cardinals.push_back(northwest);
   int northeast[2] = { 1, 1 };  cardinals.push_back(northeast);
   int southwest[2] = { -1, -1 };  cardinals.push_back(southwest);
   int southeast[2] = { 1, -1 };  cardinals.push_back(southeast);

   size = 200;

   srand(0);
   double rate = .3;
   int upperLimit = (int)(size * rate);

   //size = 9;
   for (int n = 0; n < size; n++)
   {
      int nNeighbors = rand() % upperLimit + 1;
      
      adjGraphVec[n].push_back(n); //add self to graph 
      adjGraphDistVec[n].push_back(0); //with dist 0

      for (int m = 1; m < nNeighbors; m++)
      {
         int neighbor = rand() % size;
         int distance = rand() % size + 1;


         bool neighborExists = std::find(std::begin(adjGraphVec[n]), std::end(adjGraphVec[n]), neighbor) != std::end(adjGraphVec[n]);
         if (!neighborExists) {
            adjGraphVec[n].push_back(neighbor);
            adjGraphDistVec[n].push_back(distance);

            adjGraphVec[neighbor].push_back(n);
            adjGraphDistVec[neighbor].push_back(distance);
         }
      }
   }



   idNum = index[0];


   outMessage = NULL;
   outMessage_size = 0;

   return NULL;
}

  const int Street::neighbor[8][2] = {{0,1}, {1,0}, {0,-1}, {-1,0}, {-1,1}, {1,1}, {-1,-1}, {1,-1}};



// getNeighborStatus
// based on passed in index, retrieve the status of a neighboring Grid (using outMessage)
void *Street::getShortestPath(void *argument) {


   int source = idNum;
   int* destinationTemp = (int(*))argument;
   
   int destination = intptr_t(destinationTemp);

   ostringstream convert;

   MASS_base::log(convert.str());

   bool containsVertex;
   set<int> setOfVertexes;
   int dist[size + 1];
   int prev[size + 1];

   int u = -1;

   for (int i = 0; i <= size; i++) {
      dist[i] = INT_MAX;
      prev[i] = -1;
      setOfVertexes.insert(i);
   }


   dist[source] = 0;


   ostringstream convert1;
   MASS_base::log(convert1.str());

   while (!setOfVertexes.empty()) {
      int smallest = INT_MAX;
      for (auto itr = setOfVertexes.begin(); itr != setOfVertexes.end(); ++itr) {
         if (dist[*itr] <= smallest) {
            smallest = dist[*itr];
            u = *itr;
         }
      }
      setOfVertexes.erase(u);

      int alt = 0;

      for (int i = 0; i < adjGraphVec[u].size(); i++) {
         containsVertex = setOfVertexes.find(adjGraphVec[u].at(i)) != setOfVertexes.end();
         if (containsVertex) {
            alt = dist[u] + adjGraphDistVec[u].at(i);
            if (alt < dist[adjGraphVec[u].at(i)]) {
               dist[adjGraphVec[u].at(i)] = alt;
               prev[adjGraphVec[u].at(i)] = u;
            }
         }
      }
   }

   stack<int> shortestPathStack;
   
   u = destination;
   while (prev[u] != -1) {
      shortestPathStack.push(u);
      u = prev[u];
   }
   shortestPathStack.push(u);
   int stackSize = shortestPathStack.size();
   shortestPathStack.push(stackSize);


   int shortestPath[shortestPathStack.size()];


   int counter = 0;
   while (!shortestPathStack.empty()) {

      shortestPath[counter] = shortestPathStack.top();
      counter++;

      shortestPathStack.pop();
   }

   int *retval = shortestPath;
   return retval;
}
