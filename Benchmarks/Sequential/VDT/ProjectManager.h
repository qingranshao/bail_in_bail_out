#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include "Person.h"
#include "SubFunctionLeader.h"

class ProjectManager : public Person {
  public:
    ProjectManager(int resources) : Person(40) {
      totalResources = resources;
    }
    ~ProjectManager() {
      for(int i = 0; i < teamLeaders.size(); i++) {
        teamLeaders[i] = nullptr;
      }
    }

    // Class members
    int totalResources;
    int loans;
    std::vector<SubFunctionLeader*> teamLeaders;
    std::vector<int> availableTeamLeaders;
    std::vector<int> freedTeamLeaders;
    std::vector<Person::Message*> inQueue;

    // Creates and populates a Team, adds a subFunctionLeader to PM
    void addTeam(int id, int totalPeople, int avgWage, int avgIntel, Graph::Process* current);

    // Merges from destLeader and srcLeader, while excluding srcLeader from the team
    void mergeTeams(SubFunctionLeader* srcLeader, SubFunctionLeader* destLeader);

    // Runs active teams
    void runTeams();
    void teamPostProcessing(SubFunctionLeader* leader);

    // Processes n-many Exceptions
    void processExceptions(int nExceptions);
};

#endif