#include "Graph.h"

Graph::Graph(std::string filename) {
  // Opening file
  std::ifstream infile;
  infile.open(filename, std::ifstream::in);

  // Nonexistent file
  if (!infile.is_open()) {
    std::cout << "Error opening file\n";
    exit(EXIT_FAILURE);
  }

  infile >> processesSize;           // accounts for "start" and "end"

                            // Error Handling
  if (infile.eof()) {
    return;
  }
  infile.ignore();          // throw away '\n' go to next line

                            // Initialize #size procceses
  processes = new Process*[processesSize];

  // Initializing Processes
  for (int v = 0; v < processesSize; v++) {
    // Obtaining id from file
    int id;
    infile >> id;

    // Initialize "start"/"end"
    if (v == 0) {
      processes[v] = new Process(id);
      start = processes[v];
    }
    else if (v == processesSize - 1) {
      processes[v] = new Process(id);
      end = processes[v];
    }
    else {
      int workers, rsc, intel, timespan;
      infile >> workers;
      infile >> rsc;
      infile >> intel;
      infile >> timespan;
      processes[v] =
        new Process(id, workers, rsc, intel, timespan);
      //cout << processes[v] << endl;  // TODO: WHAT IS THIS?
    }
  }

  // Establishing connections
  int to, from;
  while (!infile.eof()) {
    // Obtain path
    infile >> from;
    infile >> to;

    // Set path (prev, and next)
    processes[from]->next.push_back(processes[to]);
    processes[to]->prev.push_back(processes[from]);
  }

  // Close file
  infile.close();
}