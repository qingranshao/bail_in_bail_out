import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JFrame;

/**
 * @author Sarah Panther - adapted from Wout.java by Dr. Fukuda
 * @since 8-19-19
 */

class TBout {
    static final int defaultCellWidth = 40;
    // red background to represent human tissue
    static final Color bgColor = new Color(204, 204, 204);

    private int N = 0;              // the simulation space size
    private JFrame gWin;            // a graphics window
    private int cellWidth;          // each Place's cell's width in the window
    private Insets theInsets;       // the insets of the window

    // Macrophage colors
    private static Color macroColors[];

    // Chemokine colors
    private static Color chemoColors[];

    // T-cell color
    private static Color tCellColor = new Color(0, 0, 255);

    // Bacteria
    private static Color bacteriaColor = new Color(0, 0, 0);

    public TBout(int size) {
        N = size;
        startGraphics(N);
    }

    private void startGraphics(int N) {
        // set to cells to this size so it's easy to see what's going on
        cellWidth = defaultCellWidth;

        // initialize window and graphics:
        gWin = new JFrame("Tuberculosis");
        gWin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gWin.setLocation(50, 50);  // screen coordinates of top left corner
        gWin.setResizable(false); // false because cells don't resize themselves
        gWin.setVisible(true);    // show it!
        theInsets = gWin.getInsets();
        gWin.setSize(N * cellWidth + theInsets.left + theInsets.right,
                N * cellWidth + theInsets.top + theInsets.bottom);

        // wait for frame to get initialized
        long resumeTime = System.currentTimeMillis() + 1000;
        do {
        } while (System.currentTimeMillis() < resumeTime);

        // pop out the graphics
        Graphics g = gWin.getGraphics();
        g.setColor(bgColor);
        g.fillRect(theInsets.left,
                theInsets.top,
                N * cellWidth,
                N * cellWidth);

        // set macrophage colors - 4 to represent each state except dead
        macroColors = new Color[4];
        macroColors[0] = new Color(0, 255, 0); // Resting
        macroColors[1] = new Color(255, 222, 0);  // Infected
        macroColors[2] = new Color(102, 204, 255);  // Activated
        macroColors[3] = new Color(128,0,128);  // Chronically Infected

        // set chemokine colors - 2 colors
        chemoColors = new Color[2];
        chemoColors[0] = new Color(255, 153, 0); // chemokine level 1
        chemoColors[1] = new Color(242, 0, 86); // chemokine level 2
    }

    public void writeToGraphics(Place[] places) {
        Graphics g = gWin.getGraphics();

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                displayPlace(g, places[TBXmlParser.toPlaceID(i, j, N)], i, j);
            }
        }
    }

    public void setTitle(String iterationTitle) {
        gWin.setTitle(iterationTitle);
    }

    /**
     * Each cell that represents a Place is divided into four quadrants:
     * Quadrant 1 : a macrophage (if present) and its state
     * Quadrant 2 : bacteria (if present)
     * Quadrant 3 : T-cell (if present)
     * Quadrant 4 : chemokine level (if present)
     *
     * @param g : Graphics object from JFrame
     * @param p : Place object from xml parsing
     * @param x : x coordinate in display
     * @param y : y coordinate in display
     */
    private void displayPlace(Graphics g, Place p, int x, int y) {
//
//        // Blood vessel
//        if (p.isBVessel()) {
//            raisedEdge = true;
//        }

        // Macrophage
        if (p.isMacro()) {
            g.setColor(macroColors[p.getMacrophage().getState()]);
            // top right
            g.fill3DRect(theInsets.left + x * cellWidth + cellWidth / 2,
                    theInsets.top + y * cellWidth,
                    cellWidth / 2, cellWidth / 2, false);
        }

        // Bacteria
        if (p.isBact()) {
            g.setColor(bacteriaColor);
            // top left
            g.fill3DRect(theInsets.left + x * cellWidth,
                    theInsets.top + y * cellWidth,
                    cellWidth / 2, cellWidth / 2, false);
        }

        // T-cell
        if (p.isTCell()) {
            g.setColor(tCellColor);
            // bottom left
            g.fill3DRect(theInsets.left + x * cellWidth,
                    theInsets.top + y * cellWidth + cellWidth / 2,
                    cellWidth / 2, cellWidth / 2, false);
        }

        // Chemokine
        if (p.isChemo()) {
            g.setColor(chemoColors[p.getChemokine() - 1]);
            // bottom right
            g.fill3DRect(theInsets.left + x * cellWidth + cellWidth / 2,
                    theInsets.top + y * cellWidth + cellWidth / 2,
                    cellWidth / 2, cellWidth / 2, false);
        }
    }
}
