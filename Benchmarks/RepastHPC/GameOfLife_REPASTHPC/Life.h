

#ifndef LIFE
#define LIFE

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"


/* Agents */
class RepastHPCAgent{
	
private:
    repast::AgentId   id_;
    bool          alive;
	
public:
    RepastHPCAgent(repast::AgentId id);
	RepastHPCAgent(){}
    RepastHPCAgent(repast::AgentId id, bool alive);
	
    ~RepastHPCAgent();
	
    /* Required Getters */
    virtual repast::AgentId& getId(){                   return id_;    }
    virtual const repast::AgentId& getId() const {      return id_;    }
	
    /* Getters specific to this kind of Agent */
    bool getAlive () { return alive; }
	
    /* Setter */
    void set(int currentRank, bool newAlive);
	
    /* Actions */
    void play(repast::SharedContext<RepastHPCAgent>* context,
              repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space);    // Choose three other agents from the given context and see if they cooperate or not
  
};

/* Serializable Agent Package */
struct RepastHPCAgentPackage {
	
public:
    int    id;
    int    rank;
    int    type;
    int    currentRank;
    bool alive;
	
    /* Constructors */
    RepastHPCAgentPackage(); // For serialization
    RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank, bool _alive);
	
    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        ar & alive;
    }
	
};


#endif