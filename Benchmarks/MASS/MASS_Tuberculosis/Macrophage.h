//
// Created by sarah
//

#ifndef MACROPHAGE_H
#define MACROPHAGE_H

#include <iostream>
#include <vector>
#include <sstream>
#include "Agent.h"
#include "MASS_base.h"
#include "TBConstants.h"

class Macrophage : public Agent {

public:

    enum State {
        RESTING, INFECTED, ACTIVATED, CHRONICALLY_INFECTED, DEAD
    };

    // state functions
    static const int init_ = 0;
    static const int requestMove_ = 1;
    static const int move_ = 2;
    static const int react_ = 3;

    // for migration
    static const int addMigrationData_ = 4;
    static const int modifyMigrationData_ = 5;

    // for spawners only
    static const int spawn_ = 6;

    // for newly spawned macrophages (through blood vessels)
    static const int newMacroInit_ = 7;

    // for testing
    static const int printName_ = 8;
    static const int printLocation_ = 9;
    static const int writeMacroToXML_ = 10;

    Macrophage(void *argument) : Agent(argument) {}

    virtual void *callMethod(int functionId, void *argument) {
        switch (functionId) {
            case init_ : return init(argument);

            case requestMove_ : return requestMove();

            case move_ : return move();

            case react_ : return react(argument);

            // for spawners only
            case spawn_ : return spawnMacro();

            // for migration
            case addMigrationData_ : return addMigrationData();

            case modifyMigrationData_ : return modifyMigrationData();

            // for newly spawned macrophages
            case newMacroInit_ : return newMacroInit();

            // for testing
            case printName_ : return printName();

            case printLocation_ : return printLocation();

            case writeMacroToXML_ : return writeMacroToXML();

            default : return NULL;
        }
    };

private:

    // state functions
    void *init(void *whereToSpawnPtr);

    void *requestMove();

    void *move();

    void *addMigrationData();

    void *modifyMigrationData();

    void *react(void *day);

    void *spawnMacro();

    void *newMacroInit();

    // only one Macrophage is the spawner - Macrophage Agent ID #0
    bool isSpawner;
    // for newly spawned macrophages to be initialized
    bool isInitialized;

    State state;
    int dayInfected;
    int intracellularBacteria;

    // next state variables
    int reqPlace;

    // helper functions
    void growInfectedIntraCBact(int today);
    void growChronicIntraCBact();
    void maxChemokineCurrPlace();

    // for testing
    ostringstream convert;

    void *printName() {
        convert.str("");
        convert << agentId << endl;
        MASS_base::log(convert.str());
        return NULL;
    }

    void *printLocation() {
        if (!isSpawner) {
            convert.str("");
            convert << agentId << " at " << index[0] << " , " << index[1] << endl;
            MASS_base::log(convert.str());
        }
        return NULL;
    }

    void *writeMacroToXML();

    // migratable Macrophage data for serialization
    struct macro_migratable_data {
        // only one Macrophage is the spawner - Macrophage Agent ID #0
        bool _isSpawner;
        // for newly spawned macrophages to be initialized
        bool _isInitialized;

        State _state;
        int _dayInfected;
        int _intracellularBacteria;

        // next state variables
        int _reqPlace;
    };

};

#endif //MACROPHAGE_H

