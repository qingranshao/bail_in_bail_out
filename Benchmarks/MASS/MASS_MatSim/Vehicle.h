#ifndef VEHICLE_H
#define VEHICLE_H

#include "Agent.h"
#include "Street.h"
#include "MASS_base.h"
#include <sstream> // ostringstream
#include <cstdlib> // rand

#define STARTING_ENERGY 0	// starting agent energy

class Vehicle : public Agent {
public:

	// functionIds
	static const int initialize_ = 0;
	static const int updateAgentStatus_ = 1;
	static const int moveAgent_ = 2;
   static const int displayAgentStatus_ = 3;
   static const int updateCarInitialState_ = 4;
   static const int killAgent_ = 5;

	// constructor
   Vehicle(void *argument) : Agent(argument) {
		// constructor
	}	

	// callMethod
	virtual void *callMethod(int functionId, void *argument) {
		switch(functionId) {
			case (initialize_): return initialize(argument);
			case (updateAgentStatus_) : return updateAgentStatus(argument);
			case (moveAgent_) : return moveAgent(argument);
         case (displayAgentStatus_) : return displayAgentStatus(argument);
         case (updateCarInitialState_) : return updateCarInitialState(argument);
         case (killAgent_) : return killAgent(argument);
		}

		return NULL;
	}
private:
   int destination;
   int sourceTarget[1];
   int amountOfMoney;
   int totalGridWidth;
   int totalGridSize;
   int streetsTraversed;
	int neighborStatus[8];	// status of neighboring Grids (north->east->south->west)
   vector<int> shortestPath;

   vector<int> firmsServiced;
	static int cardinals[8][2];	// cardinal direction's relative indexes

	// functions
	void *initialize(void *argument);
	void *updateAgentStatus(void *argument);
	void *moveAgent(void *argument);
   void *displayAgentStatus(void *argument);
   void *updateCarInitialState(void *argument);
   void *killAgent(void *argument);
};

#endif