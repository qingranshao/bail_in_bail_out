#ifndef AGTS_H
#define AGTS_H

#include "Agent.h"
#include "Project.h"
#include "MASS_base.h"
#include <sstream> // ostringstream
#include <cstdlib> // rand

#define REPRODUCTION_AGE 5	// time needed to reproduce
#define STARTING_ENERGY 8	// starting agent energy

class Agts : public Agent {
public:
	int agentType;	// 0 = cleaning, 1 = infecting
   int totalProjectRequiredMoney;

	// functionIds
	static const int initialize_ = 0;
	static const int updateAgentStatus_ = 1;
	static const int moveAgent_ = 2;
	static const int killAgent_ = 3;
	static const int checkAgentStatus_ = 4;
   static const int displayAgentStatus_ = 5;
   static const int updateAgentWorkDuration_ = 6;
   static const int performWork_ = 7;
   static const int updateAgentIndexNum_ = 8;
   static const int verifyUpdateProjectResources_ = 9;
   static const int updateAgentSalaryNum_ = 10;
   static const int updateTotalMoney_ = 11;
   static const int obtainProjectRequirements_ = 12;
   static const int updateLeaderInitialState_ = 13;

	// constructor
   Agts(void *argument) : Agent(argument) {
		// constructor
   }

	// callMethod
	virtual void *callMethod(int functionId, void *argument) {
		switch(functionId) {
			case (initialize_): return initialize(argument);
			case (updateAgentStatus_) : return updateAgentStatus(argument);
			case (moveAgent_) : return moveAgent(argument);
			case (killAgent_) : return killAgent(argument);
			case (checkAgentStatus_) : return checkAgentStatus(argument);
         case (displayAgentStatus_) : return displayAgentStatus(argument);
         case (updateAgentWorkDuration_) : return updateAgentWorkDuration(argument);
         case (performWork_) : return performWork(argument);
         case (updateAgentIndexNum_) : return updateAgentIndexNum(argument);
         case (verifyUpdateProjectResources_) : return verifyUpdateProjectResources(argument);
         case (updateAgentSalaryNum_) : return updateAgentSalaryNum(argument);
         case (updateTotalMoney_) : return updateTotalMoney(argument);
         case (obtainProjectRequirements_) : return obtainProjectRequirements(argument);
         case (updateLeaderInitialState_) : return updateLeaderInitialState(argument);
		}

		return NULL;
   }
private:
	int agentEnergy;		// current energy of agent
   int numWorkers;
   int totSalary;
   //int totIntell;
   int durWork;
   int jobNum;
   int jobIndex;
	int neighborStatus[8];	
   int counter;
   int totalOverallMoney;
	static int cardinals[8][2];	// cardinal direction's relative indexes

	// functions
	void *initialize(void *argument);
	void *updateAgentStatus(void *argument);
	void *moveAgent(void *argument);
	void *killAgent(void *argument);
	void *checkAgentStatus(void *argument);
   void *displayAgentStatus(void *argument);
   void *updateAgentWorkDuration(void *argument);
   void *performWork(void *argument);
   void *updateAgentIndexNum(void *argument);
   void *verifyUpdateProjectResources(void *argument);
   void *updateAgentSalaryNum(void *argument);
   void *updateTotalMoney(void *argument);
   void *obtainProjectRequirements(void *argument);
   void *updateLeaderInitialState(void *argument);
};

#endif