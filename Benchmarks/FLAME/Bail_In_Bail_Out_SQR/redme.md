### Bail_in and Bail_out ###
This program will simulate Bail-in and Bail-out financial actives among 
in banks, workers, and firms
The whole program will contain six functional files and one `readme.md`

## Model.xml ##
This file define about the attribution of all agents and the behaviors (functions) 
of the agents. Also model.xml will standardize the format of messages, 
which will be broadcast to each agent via MPI.

## Bail_in_out_function.c and Bail_in_out_function.h ##
These two files will complete the functions of the agent.

## compile ##
This is a helper file will produce the `0.xml` file which is the initial state of 
each agent and environment. Also, the file will set up a runnable environment with
`main`

## run ##
This is a helper file to run the program with mpi servers after `compile`.

## node[x]-[x].xml ##
It is a snapshot of the program