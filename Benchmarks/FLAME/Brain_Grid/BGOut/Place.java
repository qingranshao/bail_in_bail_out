import java.util.Comparator;

/**
 * @author Sarah Panther
 * @since 8-19-19
 */

public class Place extends Agent implements Comparable<Agent>,
        Comparator<Agent> {

    private int p_id;
    private int occupyingNID;
    private boolean hasSoma;
    private double signalLevel;
    private Neuron_Type type;


    Place(int p_id, int n_id ) {
        super(p_id);
        occupyingNID = n_id;
    }

    public boolean hasSoma() {
        return hasSoma;
    }

    public void setSoma() {
        hasSoma = true;
    }

    public double getSignalLevel() {
        return signalLevel;
    }

    public void setSignalLevel(double signalLevel) {
        this.signalLevel = signalLevel;
    }

    public int getOccupyingNID() {
        return occupyingNID;
    }

    public Neuron_Type getType() {
        return type;
    }

    public void setType(Neuron_Type type) {
        this.type = type;
    }
}
