#ifndef BANK_H
#define BANK_H

#include "Agent.h"

class Bank : public Agent {
  public:
    // Constructor/Destructor
    Bank();
    Bank(std::vector<int> raw);

    // Overridden functs
    std::string initMsg(const int) override;
    std::string runAgent(const std::vector<std::string>&) override;
    std::vector<int> stripCopy() const override;

  private:
    std::vector<int> FirmAccounts;
    int NetAssetValue;
    int TotalProposedLoans;
};

#endif
