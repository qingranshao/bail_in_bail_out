//
// Created by sarah on 8/11/20.
//

#include "LifeObserver.h"

void LifeObserver::setup(Properties &prop) {

}

void LifeObserver::go() {
    if (_rank == 0) {
        cout << "Iteration : " << RepastProcess::instance()->getScheduleRunner().currentTick() << endl;
    }

    // cross process synchronization
    synchronize<AgentPackage>(*this, *this, *this, RepastProcess::USE_CURRENT);

    // get LifePlace patches and call check and react on them
    AgentSet<LifePlace> life;
    patches(life);

    // display state
    if (_rank == 0) {
//        life.ask(&LifePlace::printState);
    }

    life.ask(&LifePlace::checkNeighbors);

    life.ask(&LifePlace::react);
}

void LifeObserver::provideContent(const AgentRequest &request, vector<AgentPackage> &out) {
    const vector<AgentId> &ids = request.requestedAgents();
    int n = ids.size();
    for (int i = 0; i < n; i++) {
        AgentId id = ids[i];
        AgentPackage content = { id.id(), id.startingRank(), id.agentType(), id.currentRank(), false };

        LifePlace* life = who<LifePlace>(id);
        content.alive = life->isAlive();
        out.push_back(content);
    }
}

RelogoAgent *LifeObserver::createAgent(const AgentPackage &content) {
    return new LifePlace(content.getId(), this, content);
}

void LifeObserver::createAgents(vector<AgentPackage> &contents, vector<RelogoAgent *> &outgoing) {
    size_t size = contents.size();
    for (size_t i = 0; i < size; ++i) {
        AgentPackage aPackage = contents[i];
        //outgoing.push_back(new Patch(aPackage.getId(), this));
        outgoing.push_back(new LifePlace(aPackage.getId(), this, aPackage));
    }
}

void LifeObserver::provideContent(RelogoAgent *relogoAgent, vector<AgentPackage> &outgoing) {
    LifePlace *life = static_cast<LifePlace *>(relogoAgent);    // all agents are LifePlace
    AgentId id = life->getId();

    AgentPackage aPackage = {id.id(), id.startingRank(), id.agentType(), id.currentRank(), life->isAlive()};
    outgoing.push_back(aPackage);
}

void LifeObserver::updateAgent(AgentPackage agentPackage) {
    repast::AgentId id(agentPackage.id, agentPackage.proc, agentPackage.type);
    LifePlace *life = who<LifePlace>(id);
    life->setAlive(agentPackage.alive);
}

